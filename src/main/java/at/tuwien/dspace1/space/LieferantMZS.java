package at.tuwien.dspace1.space;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.mozartspaces.capi3.Coordinator;
import org.mozartspaces.capi3.KeyCoordinator;
import org.mozartspaces.capi3.LabelCoordinator;
import org.mozartspaces.capi3.Selector;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.MzsConstants.RequestTimeout;

import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.BestandteilFactory;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.shared.ILieferant;
import at.tuwien.dspace1.shared.Util;

public class LieferantMZS implements ILieferant {

	private BestandteilTyp typ;
	private int anzahl;
	private final UUID lieferantId; // TODO change to consolen argument
//	private static long lieferant_counter = 0;
//	private long lieferantId;

	private Random rand = new Random();

	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference assemblyRef;

	public LieferantMZS(BestandteilTyp typ, int anzahl) {
//		System.out.println("instantinating lieferant with " + anzahl + " "
//				+ typ.name());
		this.typ = typ;
		this.anzahl = anzahl;
		lieferantId = UUID.randomUUID();
//		lieferantId = ++lieferant_counter;
//
		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

		try {
			assemblyRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.ASSEMBLY_CONTAINER, capi);
		} catch (MzsCoreException e) {

		}
	}

	public void processLieferung() {
		// System.out.println("writing like a rocket into the space");

		for (int i = 0; i < this.anzahl; ++i) {
			Bestandteil part = BestandteilFactory.createPart(
					lieferantId, typ);

			Entry entry = new Entry(part,
					TypeCoordinator.newCoordinationData());
			
			if(!Util.debugMode()) {
				int time = rand.nextInt(2000);
				time = (time < 1000) ? 1000 : time;
				try {
					Thread.sleep(time);
				} catch (InterruptedException e) {
				}
			}

			try {
				capi.write(entry, assemblyRef, RequestTimeout.TRY_ONCE, null);
			} catch (MzsCoreException e) {
				e.printStackTrace();
			}
		}
		
	}
	
//	public void fastProcessLieferung() {
//		/* this is a much faster approach */
//		/* unfortunately not used for the exercise */
//		List<Entry> entries = new ArrayList<Entry>();
//		for (int i = 0; i < this.anzahl; ++i) {
//			Bestandteil spaceShuttle = BestandteilFactory.createPart(
//					lieferantId, typ);
//
//			Entry entry = new Entry(spaceShuttle,
//					TypeCoordinator.newCoordinationData());
//			entries.add(entry);
//			
//			// ausladen in fabrikslager dauert ein bissi was
//			if (!Util.debugMode()) {
//				int time = rand.nextInt(2000);
//				time = (time < 1000) ? 1000 : time;
//
//				try {
//					Thread.sleep(time);
//				} catch (InterruptedException e) {
//					System.out
//							.println("I WAS ASLEEP AND YOU INTERRUPTED ME");
//				}
//			}
//		}
//		
//		try {
//			capi.write(entries, assemblyRef, RequestTimeout.TRY_ONCE, null);
//		} catch (MzsCoreException e) {
//			e.printStackTrace();
//		}
//	}

	public BestandteilTyp getTyp() {
		return typ;
	}

	public void setTyp(BestandteilTyp typ) {
		this.typ = typ;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

}
