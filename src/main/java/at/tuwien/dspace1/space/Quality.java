package at.tuwien.dspace1.space;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

import org.mozartspaces.capi3.CoordinationData;
import org.mozartspaces.capi3.CountNotMetException;
import org.mozartspaces.capi3.Query;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.TransactionReference;
import org.mozartspaces.core.MzsConstants.RequestTimeout;

import at.tuwien.dspace1.parts.Armband;
import at.tuwien.dspace1.parts.Gehause;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.Uhrwerk;
import at.tuwien.dspace1.parts.Zeiger;
import at.tuwien.dspace1.shared.Util;

public class Quality {
	private Random rand = new Random();

	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference qualityRef;
	private ContainerReference logisticRef;
	private ContainerReference benchmarkRef;

	private long id;

	public Quality(long id) {
		this.id = id;

		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

		try {
			qualityRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.QUALITY_CONTAINER, capi);
			logisticRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.LOGISTIC_CONTAINER, capi);
			benchmarkRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.BENCHMARK_CONTAINER, capi);
		} catch (MzsCoreException e) {

		}

	}
	
	private void waitSignal() {
		try {
			capi.test(benchmarkRef,
					TypeCoordinator.newSelector(Boolean.class, 1),
					MzsConstants.RequestTimeout.INFINITE, null);
		} catch (MzsCoreException e1) {
			e1.printStackTrace();
			return;
		}
	}

	public void processQualityControl() {
		System.out.println("doing quality control of clocks..");

		waitSignal();

		TransactionReference tx;
		try {
			tx = capi.createTransaction(10000, spaceURI);

			try {
				try {
					ArrayList<Serializable> s_uhren = fetchPart(Uhr.class, tx, 1);

					Uhr uhr = (Uhr) s_uhren.get(0);

					uhr.setQuality(rand.nextInt(11));
					uhr.setQualityId(this.id);

					Util.writeClock(uhr, capi, logisticRef, tx);

					capi.commitTransaction(tx);
				} catch (CountNotMetException discardMe) {
					capi.rollbackTransaction(tx);
				}
				
			} catch (MzsCoreException ex) {
				capi.rollbackTransaction(tx);
				ex.printStackTrace();
			}
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}

	private ArrayList<Serializable> fetchPart(Class<?> T,
			TransactionReference tx, int count) throws MzsCoreException {
		ArrayList<Serializable> resultentries = capi.take(qualityRef,
				TypeCoordinator.newSelector(T, count),
				MzsConstants.RequestTimeout.INFINITE, tx);

		System.out.println("taken results (size: " + resultentries.size()
				+ ") out of space");
		return resultentries;
	}

	public static void main(String[] args) {
		System.out.println("starting quality worker...");

		if (args.length != 1) {
			usage();
		}

		Quality qual = new Quality(Long.parseLong(args[0]));

		while(true) {
			qual.processQualityControl();
		}

		//System.out.println("END");

		// TODO: maybe delete this later
		//System.exit(0);
	}

	private static void usage() {
		System.out.println("Usage: Quality <ID>");
		System.exit(-1);
	}
}
