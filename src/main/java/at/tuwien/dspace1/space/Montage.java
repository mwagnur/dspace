package at.tuwien.dspace1.space;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.mozartspaces.capi3.ComparableProperty;
import org.mozartspaces.capi3.Coordinator;
import org.mozartspaces.capi3.CountNotMetException;
import org.mozartspaces.capi3.KeyCoordinator;
import org.mozartspaces.capi3.LabelCoordinator;
import org.mozartspaces.capi3.Query;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.Selector;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.TransactionReference;
import org.mozartspaces.core.MzsConstants.RequestTimeout;

import at.tuwien.dspace1.parts.*;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.Auftrag.Priority;
import at.tuwien.dspace1.shared.Util;

public class Montage {
	private Random rand = new Random();

	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference assemblyRef;
	private ContainerReference qualityRef;
	private ContainerReference benchmarkRef;
	private ContainerReference auftragRef;

	private long id;
	
	private boolean lastJobWasContract = false;

	public Montage(long id) {
		this.id = id;

		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

		try {
			assemblyRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.ASSEMBLY_CONTAINER, capi);
			qualityRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.QUALITY_CONTAINER, capi);
			benchmarkRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.BENCHMARK_CONTAINER, capi);
			auftragRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.AUFTRAG_CONTAINER, capi);
		} catch (MzsCoreException e) {

		}
	}
	
	private void waitSignal() {
		try {
			capi.test(benchmarkRef,
					TypeCoordinator.newSelector(Boolean.class, 1),
					MzsConstants.RequestTimeout.INFINITE, null);
		} catch (MzsCoreException e1) {
			e1.printStackTrace();
			return;
		}
	}

	public void processClockBuilding() {
		System.out.println("trying to take results..");
		boolean didSomeWork = false;
		/* wait for start signal */
		waitSignal();

		if(!didSomeWork) {
			try {
				didSomeWork = tryPriority(Priority.HIGH);
			} catch (MzsCoreException e) {
				e.printStackTrace();
			}
		}
		
		if(!didSomeWork) {
			try {
				didSomeWork = tryPriority(Priority.NORMAL);
			} catch (MzsCoreException e) {
				e.printStackTrace();
			}
		}
		
		if(!didSomeWork) {
			if(!lastJobWasContract) {
				try {
					didSomeWork = tryPriority(Priority.LOW);
				} catch (MzsCoreException e) {
					e.printStackTrace();
				}
				lastJobWasContract = true;
			} else {
				TransactionReference tx;
				try {
					tx = capi.createTransaction(10000, spaceURI);
					try {
						makeSomeRandomClock(tx);
						capi.commitTransaction(tx);
					} catch (MzsCoreException ex) {
						capi.rollbackTransaction(tx);
						ex.printStackTrace();
					}
				} catch (MzsCoreException e) {
					e.printStackTrace();
				}
				lastJobWasContract = false;
			}
		}
	}

	private boolean tryPriority(Priority prio) throws MzsCoreException {
		TransactionReference tx;
		try {
			tx = capi.createTransaction(10000, spaceURI);
			try {
				Auftrag a = checkPriorityWork(prio, tx);
				
				handleAuftrag(a, tx);
				capi.commitTransaction(tx);
				return true;
			} catch (CountNotMetException ex) {
				capi.rollbackTransaction(tx);
				ex.printStackTrace();
			}
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private void handleAuftrag(Auftrag a, TransactionReference tx)
			throws MzsCoreException{
		ArrayList<Integer> possibilities = new ArrayList<Integer>();
		// check what can be done
		if(a.getKlassischeUhren() > 0) {
			possibilities.add(0);
		}
		if(a.getNormaleSportUhren() > 0) {
			possibilities.add(1);
		}
		if(a.getZweiZonenSportUhren() > 0) {
			possibilities.add(2);
		}
		// do what can be done
		Auftrag changedA = doWhatYouCan(a, possibilities, tx);
		// ?? mark auftrag done maybe
		if ((changedA.getKlassischeUhren() < 1) 
				&& (changedA.getKlassischeUhren() < 1) 
				&& (changedA.getKlassischeUhren() < 1)) {
			changedA.markCompleted();
		}
		// send back changed auftrag
		Util.writeAuftrag(changedA, capi, auftragRef, tx);
	}
	
	private Auftrag doWhatYouCan(Auftrag a, ArrayList<Integer> possibilities,
			TransactionReference tx) throws MzsCoreException {
		if(possibilities.size() < 1) {
			/* NOTHING TO DO HERE */
			/* WE HAVE NO CHOICE  */
			try {
				// 100ms coffee break if there are no possibilities
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
			return a;
		}
		
		switch(possibilities.get(rand.nextInt(possibilities.size()))) {
		case 0:
			makeClassicClock(a, tx);
			if (a != null) {
				a.setKlassischeUhren(a.getKlassischeUhren()-1);
			}
			break;
		case 1:
			makeNormalSportClock(a, tx);
			if (a != null) {
				a.setNormaleSportUhren(a.getNormaleSportUhren()-1);
			}
			break;
		case 2:
			make2ZoneSportClock(a, tx);
			if (a != null) {
				a.setZweiZonenSportUhren(a.getZweiZonenSportUhren()-1);
			}
			break;
		default:
			break;
		}
		
		return a;
	}
	
	private Auftrag checkPriorityWork(Priority prio, TransactionReference tx) 
			throws MzsCoreException {
		ComparableProperty priority = ComparableProperty.forName("priority");
		ComparableProperty completed = ComparableProperty.forName("completed");
		
		Query query = new Query();
		query.filter(completed.equalTo(false));
		query.filter(priority.equalTo(prio));
		
		ArrayList<Serializable> resultentries = capi.take(auftragRef,
				QueryCoordinator.newSelector(query, 1),
				MzsConstants.RequestTimeout.TRY_ONCE, tx);
		return ((Auftrag) resultentries.get(0));
	}
	
	private void makeSomeRandomClock(TransactionReference tx) throws MzsCoreException {
		/*
		 * KlassischeUhr = 0
		 * NormaleSportUhr = 1
		 * ZweiZonenSportUhr = 2
		 */
		ArrayList<Integer> possibilities = new ArrayList<Integer>();

		long gehause = capi.test(assemblyRef, TypeCoordinator.newSelector(
				Gehause.class, MzsConstants.Selecting.COUNT_MAX),
				MzsConstants.RequestTimeout.ZERO, null);
		long uhrwerk = capi.test(assemblyRef, TypeCoordinator.newSelector(
				Uhrwerk.class, MzsConstants.Selecting.COUNT_MAX),
				MzsConstants.RequestTimeout.ZERO, null);
		long zeiger = capi.test(assemblyRef, TypeCoordinator.newSelector(
				Zeiger.class, MzsConstants.Selecting.COUNT_MAX),
				MzsConstants.RequestTimeout.ZERO, null);
		long lederArmband = capi.test(assemblyRef, TypeCoordinator.newSelector(
				LederArmband.class, MzsConstants.Selecting.COUNT_MAX),
				MzsConstants.RequestTimeout.ZERO, null);
		long metallArmband = capi.test(assemblyRef, TypeCoordinator.newSelector(
				MetallArmband.class, MzsConstants.Selecting.COUNT_MAX),
				MzsConstants.RequestTimeout.ZERO, null);

		if( (gehause > 0) && (uhrwerk > 0) && (zeiger > 1)) {
			if (lederArmband > 0) {
				// KlassischeUhr = 0q
				possibilities.add(0);
			}
			if (metallArmband > 0) {
				// NormaleSportUhr = 1
				possibilities.add(1);
				
				if(zeiger > 2) {
					// ZweiZonenSportUhr = 2
					possibilities.add(2);
				}
			}
		}

		doWhatYouCan(null, possibilities, tx);
	}
	
	private void makeClassicClock(Auftrag auftrag, TransactionReference tx) 
			throws MzsCoreException {
		try {
			ArrayList<Serializable> s_armband = fetchPart(LederArmband.class,
					tx, 1);
			ArrayList<Serializable> s_gehause = fetchPart(Gehause.class,
					tx, 1);
			ArrayList<Serializable> s_uhrwerk = fetchPart(Uhrwerk.class,
					tx, 1);
			ArrayList<Serializable> s_zeiger = fetchPart(Zeiger.class, tx,
					2);
			
			LederArmband a = (LederArmband) s_armband.get(0);
			Gehause g = (Gehause) s_gehause.get(0);
			Uhrwerk u = (Uhrwerk) s_uhrwerk.get(0);
			Zeiger z1 = (Zeiger) s_zeiger.get(0);
			Zeiger z2 = (Zeiger) s_zeiger.get(1);

			Uhr uhr = new KlassischeUhr(a, g, u, z1, z2);
			uhr.setSerialNo(this.nextSerialNo(tx));
			uhr.setMontageId(this.id);
			if (auftrag != null){
				uhr.setContractNo(auftrag.getId());
			}
			
			randomSleep();
			
			Util.writeClock(uhr, capi, qualityRef, tx);

		} catch (CountNotMetException discardMe) {
			capi.rollbackTransaction(tx);
		}
	}
	
	private void makeNormalSportClock(Auftrag auftrag, TransactionReference tx) 
			throws MzsCoreException {
		try {
			ArrayList<Serializable> s_armband = fetchPart(MetallArmband.class,
					tx, 1);
			ArrayList<Serializable> s_gehause = fetchPart(Gehause.class,
					tx, 1);
			ArrayList<Serializable> s_uhrwerk = fetchPart(Uhrwerk.class,
					tx, 1);
			ArrayList<Serializable> s_zeiger = fetchPart(Zeiger.class, tx,
					2);
			
			MetallArmband a = (MetallArmband) s_armband.get(0);
			Gehause g = (Gehause) s_gehause.get(0);
			Uhrwerk u = (Uhrwerk) s_uhrwerk.get(0);
			Zeiger z1 = (Zeiger) s_zeiger.get(0);
			Zeiger z2 = (Zeiger) s_zeiger.get(1);

			Uhr uhr = new NormaleSportUhr(a, g, u, z1, z2);
			uhr.setSerialNo(this.nextSerialNo(tx));
			uhr.setMontageId(this.id);
			if (auftrag != null){
				uhr.setContractNo(auftrag.getId());
			}
			
			randomSleep();
			
			Util.writeClock(uhr, capi, qualityRef, tx);
			
		} catch (CountNotMetException discardMe) {
			capi.rollbackTransaction(tx);
		}
	}
	
	private void make2ZoneSportClock(Auftrag auftrag, TransactionReference tx) 
			throws MzsCoreException {
		try {
			ArrayList<Serializable> s_armband = fetchPart(MetallArmband.class,
					tx, 1);
			ArrayList<Serializable> s_gehause = fetchPart(Gehause.class,
					tx, 1);
			ArrayList<Serializable> s_uhrwerk = fetchPart(Uhrwerk.class,
					tx, 1);
			ArrayList<Serializable> s_zeiger = fetchPart(Zeiger.class, tx,
					3);
			
			MetallArmband a = (MetallArmband) s_armband.get(0);
			Gehause g = (Gehause) s_gehause.get(0);
			Uhrwerk u = (Uhrwerk) s_uhrwerk.get(0);
			Zeiger z1 = (Zeiger) s_zeiger.get(0);
			Zeiger z2 = (Zeiger) s_zeiger.get(1);
			Zeiger z3 = (Zeiger) s_zeiger.get(2);

			Uhr uhr = new ZweiZonenSportUhr(a, g, u, z1, z2, z3);
			uhr.setSerialNo(this.nextSerialNo(tx));
			uhr.setMontageId(this.id);
			if (auftrag != null){
				uhr.setContractNo(auftrag.getId());
			}
			
			randomSleep();
			
			Util.writeClock(uhr, capi, qualityRef, tx);

		} catch (CountNotMetException discardMe) {
			capi.rollbackTransaction(tx);
		}
	}
	
	private void randomSleep() {
		// ausladen in completed lager dauert ein bissi was
		if (!Util.debugMode()) {
			int time = rand.nextInt(3000);
			time = (time < 1000) ? 1000 : time;

			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				System.out.println("I WAS ASLEEP AND YOU INTERRUPTED ME");
			}
		}
	}

	private ArrayList<Serializable> fetchPart(Class<?> T,
			TransactionReference tx, int count) throws MzsCoreException {
		ArrayList<Serializable> resultentries = capi.take(assemblyRef,
				TypeCoordinator.newSelector(T, count),
				MzsConstants.RequestTimeout.INFINITE, tx);

		System.out.println("taken results (size: " + resultentries.size()
				+ ") out of space");
		return resultentries;
	}

	private int nextSerialNo(TransactionReference tx) throws MzsCoreException {
		int id = 1;
		try {
			ArrayList<Serializable> resultentries = capi.take(qualityRef,
					TypeCoordinator.newSelector(Integer.class, 1),
					MzsConstants.RequestTimeout.TRY_ONCE, tx);

			try {
				id = (Integer) resultentries.get(0);
			} catch (Exception e) {

			}

			++id;

		} catch (CountNotMetException e) {
			/* new entry created below */
		}

		Entry entry = new Entry(id, TypeCoordinator.newCoordinationData());
		capi.write(qualityRef, RequestTimeout.TRY_ONCE, tx, entry);

		return id;
	}

	public static void main(String[] args) {
		System.out.println("starting montage worker...");

		if (args.length != 1) {
			usage();
		}

		Montage mon = new Montage(Long.parseLong(args[0]));

		while(true) {
			mon.processClockBuilding();
		}

	}

	private static void usage() {
		System.out.println("Usage: Montage <ID>");
		System.exit(-1);
	}

}
