package at.tuwien.dspace1.space;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Random;

import org.mozartspaces.capi3.ComparableProperty;
import org.mozartspaces.capi3.Coordinator;
import org.mozartspaces.capi3.CountNotMetException;
import org.mozartspaces.capi3.Property;
import org.mozartspaces.capi3.Query;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.Selector;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.TransactionReference;
import org.mozartspaces.core.MzsConstants.RequestTimeout;
import org.mozartspaces.util.parser.sql.javacc.ParseException;

import at.tuwien.dspace1.parts.Armband;
import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.Gehause;
import at.tuwien.dspace1.parts.KlassischeUhr;
import at.tuwien.dspace1.parts.LederArmband;
import at.tuwien.dspace1.parts.MetallArmband;
import at.tuwien.dspace1.parts.NormaleSportUhr;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.Uhrwerk;
import at.tuwien.dspace1.parts.Zeiger;
import at.tuwien.dspace1.parts.ZweiZonenSportUhr;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.GrossAuftrag;
import at.tuwien.dspace1.shared.Util;
import at.tuwien.dspace1.shared.Auftrag.Priority;

public class Kundenbetreuer {
	private Random rand = new Random();
	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference finalRef;
	private ContainerReference haendlerRef;

	private long id;


	public Kundenbetreuer(long id) {
		this.id = id;

		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

		try {

			this.finalRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.FINAL_CONTAINER, capi);
			this.haendlerRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.HAENDLER_CONTAINER, capi);

		} catch (MzsCoreException e) {

		}
	}
	
	public void process() {
		TransactionReference tx;
		try {
			tx = capi.createTransaction(10000, spaceURI);
			try {
				GrossAuftrag a = getRequest(tx);
				
				handleAuftrag(a, tx);
				capi.commitTransaction(tx);
			} catch (CountNotMetException ex) {
				capi.rollbackTransaction(tx);
				ex.printStackTrace();
			}
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}
	
	private GrossAuftrag getRequest(TransactionReference tx) 
			throws MzsCoreException {
		ComparableProperty done = ComparableProperty.forName("isDone");
		
		Query query = new Query();
		query.filter(done.equalTo(false));
		
		ArrayList<Serializable> resultentries = capi.take(haendlerRef,
				QueryCoordinator.newSelector(query, 1),
				MzsConstants.RequestTimeout.TRY_ONCE, tx);
		return ((GrossAuftrag) resultentries.get(0));
	}
	
	private void handleAuftrag(GrossAuftrag a, TransactionReference tx)
			throws MzsCoreException{
		ArrayList<Integer> possibilities = new ArrayList<Integer>();
		// check what can be done
		if(a.getKlassischeUhren() > 0) {
			possibilities.add(0);
		}
		if(a.getNormaleSportUhren() > 0) {
			possibilities.add(1);
		}
		if(a.getZweiZonenSportUhren() > 0) {
			possibilities.add(2);
		}
		// do what can be done
		GrossAuftrag changedA = doWhatYouCan(a, possibilities, tx);
		// ?? mark auftrag done maybe
		if ((changedA.getKlassischeUhren() < 1) 
				&& (changedA.getKlassischeUhren() < 1) 
				&& (changedA.getKlassischeUhren() < 1)) {
			changedA.setDone(true);
		}
		// send back changed auftrag
		Util.writeGrossAuftrag(changedA, capi, haendlerRef, tx);
	}
	
	
	
	private GrossAuftrag doWhatYouCan(GrossAuftrag a, ArrayList<Integer> possibilities,
			TransactionReference tx) throws MzsCoreException {
		if(possibilities.size() < 1) {
			/* NOTHING TO DO HERE */
			/* WE HAVE NO CHOICE  */
			try {
				// 100ms coffee break if there are no possibilities
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
			return a;
		}
		
		switch(possibilities.get(rand.nextInt(possibilities.size()))) {
		case 0:
			sendClassicClock(a, tx);
			System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>> SENT CLOCK");
			if (a != null) {
				a.setKlassischeUhren(a.getKlassischeUhren()-1);
			}
			break;
		case 1:
			sendNormalSportClock(a, tx);
			System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>> SENT CLOCK");
			if (a != null) {
				a.setNormaleSportUhren(a.getNormaleSportUhren()-1);
			}
			break;
		case 2:
			send2ZoneSportClock(a, tx);
			System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>> SENT CLOCK");
			if (a != null) {
				a.setZweiZonenSportUhren(a.getZweiZonenSportUhren()-1);
			}
			break;
		default:
			break;
		}
		
		return a;
	}

	
	private void sendClassicClock(GrossAuftrag auftrag, TransactionReference tx) 
			throws MzsCoreException {
		try {
			ArrayList<Serializable> uhren = fetchPart(KlassischeUhr.class,
					tx, 1);
			
			KlassischeUhr u = (KlassischeUhr) uhren.get(0);

			Util.writeClock(u, capi, auftrag.getCref(), null);

		} catch (CountNotMetException discardMe) {

		}
	}
	
	private void sendNormalSportClock(GrossAuftrag auftrag, TransactionReference tx) 
			throws MzsCoreException {
		try {
			ArrayList<Serializable> uhren = fetchPart(NormaleSportUhr.class,
					tx, 1);
			
			NormaleSportUhr u = (NormaleSportUhr) uhren.get(0);

			Util.writeClock(u, capi, auftrag.getCref(), null);

		} catch (CountNotMetException discardMe) {

		}
	}
	
	private void send2ZoneSportClock(GrossAuftrag auftrag, TransactionReference tx) 
			throws MzsCoreException {
		try {
			ArrayList<Serializable> uhren = fetchPart(ZweiZonenSportUhr.class,
					tx, 1);
			
			ZweiZonenSportUhr u = (ZweiZonenSportUhr) uhren.get(0);

			Util.writeClock(u, capi, auftrag.getCref(), null);

		} catch (CountNotMetException discardMe) {

		}
	}
	
	private ArrayList<Serializable> fetchPart(Class<?> T,
			TransactionReference tx, int count)
			throws MzsCoreException {
		
		ComparableProperty auftrag = ComparableProperty.forName("contractNo");
		Query query = new Query().filter(auftrag.equalTo(-1));

		ArrayList<Selector> coords = new ArrayList<Selector>();
		coords.add(TypeCoordinator.newSelector(T, 1));
		coords.add(QueryCoordinator.newSelector(query));
		
		ArrayList<Serializable> resultentries = capi.take(finalRef,
				coords,
				MzsConstants.RequestTimeout.TRY_ONCE, tx);

		System.out.println("taken results (size: " + resultentries.size()
				+ ") out of space");

		return resultentries;
	}

	public static void main(String[] args) {
		System.out.println("starting kundenbetreuer...");

		if (args.length != 1) {
			usage();
		}

		Kundenbetreuer betr = new Kundenbetreuer(Long.parseLong(args[0]));
		
		while(true) {
			betr.process();

		}

	}

	private static void usage() {
		System.out.println("Usage: Kundenbetreuer <ID>");
		System.exit(-1);
	}

}
