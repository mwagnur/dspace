package at.tuwien.dspace1.space;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.mozartspaces.capi3.ComparableProperty;
import org.mozartspaces.capi3.CountNotMetException;
import org.mozartspaces.capi3.Property;
import org.mozartspaces.capi3.Query;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.TransactionReference;
import org.mozartspaces.core.MzsConstants.RequestTimeout;
import org.mozartspaces.util.parser.sql.javacc.ParseException;

import at.tuwien.dspace1.parts.Armband;
import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.Gehause;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.Uhrwerk;
import at.tuwien.dspace1.parts.Zeiger;
import at.tuwien.dspace1.shared.Util;

public class Logistik {
	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference assemblyRef;
	private ContainerReference logisticRef;
	private ContainerReference finalRef;
	private ContainerReference trashRef;
	private ContainerReference benchmarkRef;

	private Logistik.LogType type;
	private long id;

	public enum LogType {
		A, B
	};

	public Logistik(long id, LogType type) {
		this.id = id;
		this.type = type;

		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

		try {
			assemblyRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.ASSEMBLY_CONTAINER, capi);
			logisticRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.LOGISTIC_CONTAINER, capi);
			finalRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.FINAL_CONTAINER, capi);
			trashRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.TRASH_CONTAINER, capi);
			benchmarkRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.BENCHMARK_CONTAINER, capi);
		} catch (MzsCoreException e) {

		}
	}
	
	private void waitSignal() {
		try {
			capi.test(benchmarkRef,
					TypeCoordinator.newSelector(Boolean.class, 1),
					MzsConstants.RequestTimeout.INFINITE, null);
		} catch (MzsCoreException e1) {
			e1.printStackTrace();
			return;
		}
	}

	public void process() {
		System.out.println("doing logistics for clocks..");
		
		waitSignal();

		TransactionReference tx;
		try {
			tx = capi.createTransaction(10000, spaceURI);

			try {
				ArrayList<Serializable> s_uhren = fetchPart(Uhr.class, tx, 1,
						false);

				/* got a good clock */
				Uhr uhr = (Uhr) s_uhren.get(0);
				uhr.setLogistikId(this.id);

				Util.writeClock(uhr, capi, finalRef, tx);
				capi.commitTransaction(tx);

			} catch (CountNotMetException e) {

				/* there was no quality clock at the time */
				/* check for a junk clock and disassemble it */
				capi.rollbackTransaction(tx);

				TransactionReference tx_junk = capi.createTransaction(
						10000, spaceURI);
				try {
					ArrayList<Serializable> s_uhren = fetchPart(Uhr.class,
							tx_junk, 1, true);

					if (s_uhren.size() == 0) {
						/* empty spaces */
						return;
					}

					Uhr uhr = (Uhr) s_uhren.get(0);
					uhr.setLogistikId(this.id);
					Util.writeClock(uhr, capi, trashRef, tx_junk);

					Armband a = uhr.getArmband();
					Gehause g = uhr.getGehause();
					// uhrwerk thrown away
					//Uhrwerk u = uhr.getUhrwerk();
					Zeiger z1 = uhr.getZeig1();
					Zeiger z2 = uhr.getZeig2();

					returnParts(a, g, z1, z2, tx_junk);
					capi.commitTransaction(tx_junk);
				} catch (CountNotMetException discardMe) {
					System.out
							.println("INFO: there was nothing for me in the space");
					capi.rollbackTransaction(tx_junk);
				}

			} catch (MzsCoreException ex) {
				capi.rollbackTransaction(tx);
				ex.printStackTrace();
			}
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}

	private ArrayList<Serializable> fetchPart(Class<?> T,
			TransactionReference tx, int count, boolean getJunk)
			throws MzsCoreException {
		ComparableProperty quality = ComparableProperty.forName("quality");

		Query query = null;

		if (!getJunk) {
			if (this.type.equals(Logistik.LogType.A)) {
				query = new Query().filter(quality.between(8, 10));
			} else {
				query = new Query().filter(quality.between(3, 7));
			}
		} else {
			query = new Query().filter(quality.between(0, 2));
		}

		ArrayList<Serializable> resultentries = capi.take(logisticRef,
				QueryCoordinator.newSelector(query),
				MzsConstants.RequestTimeout.TRY_ONCE, tx);

		System.out.println("taken results (size: " + resultentries.size()
				+ ") out of space");

		return resultentries;
	}

	private void returnParts(Armband a, Gehause g, Zeiger z1, Zeiger z2,
			TransactionReference tx) throws MzsCoreException {
		System.out.println("writing like a rocket into the space");

		Entry armband = new Entry(a, TypeCoordinator.newCoordinationData());
		Entry gehause = new Entry(g, TypeCoordinator.newCoordinationData());
		Entry zeiger1 = new Entry(z1, TypeCoordinator.newCoordinationData());
		Entry zeiger2 = new Entry(z2, TypeCoordinator.newCoordinationData());

		capi.write(assemblyRef, RequestTimeout.TRY_ONCE, tx, armband);
		capi.write(assemblyRef, RequestTimeout.TRY_ONCE, tx, gehause);
		capi.write(assemblyRef, RequestTimeout.TRY_ONCE, tx, zeiger1);
		capi.write(assemblyRef, RequestTimeout.TRY_ONCE, tx, zeiger2);

		System.out.println("apparently written into the space!");
	}

	public static void main(String[] args) {
		System.out.println("starting Logistik worker...");

		if (args.length != 2) {
			usage();
		}
		if (!(args[1].equalsIgnoreCase("A"))
				&& !(args[1].equalsIgnoreCase("B"))) {
			usage();
		}

		Logistik log;
		if (args[1].equalsIgnoreCase("B")) {
			log = new Logistik(Long.parseLong(args[0]), Logistik.LogType.B);
		} else {
			log = new Logistik(Long.parseLong(args[0]), Logistik.LogType.A);
		}

		while(true) {
			log.process();
//			try {
//				Thread.sleep(200);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
		}

		//System.out.println("END");

		// TODO: maybe delete this later
		//System.exit(0);
	}

	private static void usage() {
		System.out.println("Usage: Logistik <ID> <typ>");
		System.exit(-1);
	}

}
