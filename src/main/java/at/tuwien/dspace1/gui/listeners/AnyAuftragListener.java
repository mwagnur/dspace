package at.tuwien.dspace1.gui.listeners;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.Entry;
import org.mozartspaces.notifications.Notification;
import org.mozartspaces.notifications.NotificationListener;
import org.mozartspaces.notifications.Operation;

import at.tuwien.dspace1.parts.*;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.Util;

public class AnyAuftragListener implements NotificationListener {
	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference anyRef;

	private long auftraege = 0;
	HashMap<Integer, Auftrag> auftragList = new HashMap<Integer, Auftrag>();

	public AnyAuftragListener(String containerName) {
		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

			try {
				this.anyRef = Util.getOrCreateNamedContainer(spaceURI,
						containerName, capi);
			} catch (MzsCoreException e) {
				e.printStackTrace();
			}

		try {

			ArrayList<Serializable> tmp = capi.read(anyRef, TypeCoordinator
					.newSelector(Auftrag.class, MzsConstants.Selecting.COUNT_MAX),
					MzsConstants.RequestTimeout.ZERO, null);

			for (Serializable s : tmp) {
				Auftrag a = (Auftrag) s;
				this.auftraege++;
				auftragList.put(a.getId(), a);
			}

			if (Util.debugMode()) {
				System.err.println("Initial data: Uhren: " + this.auftraege);
			}

		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void entryOperationFinished(Notification arg0, Operation arg1,
			List<? extends Serializable> arg2) {
		for (Serializable s : arg2) {

			switch (arg1) {
			case DELETE:
				if (s instanceof Auftrag) {
					Auftrag a = (Auftrag) s;
					auftragList.remove(a.getId());
					this.auftraege--;
				}
				break;
			case READ:
				break;
			case TAKE:
				if (s instanceof Auftrag) {
					Auftrag a = (Auftrag) s;
					auftragList.remove(a.getId());
					this.auftraege--;
				}
				break;
			case TEST:
				break;
			case WRITE:
				Entry e = null;
				if (s instanceof Entry) {
					e = (Entry) s;
				}
				if ((e != null) && e.getValue() instanceof Auftrag) {
					Auftrag a = (Auftrag) e.getValue();
					auftragList.put(a.getId(), a);
					this.auftraege++;
				}
				break;
			default:
				break;
			}
		}
	}

	public long getUhren() {
		return this.auftraege;
	}

	public List<Auftrag> getActiveAuftragList() {
		List<Auftrag> tmp = new ArrayList<Auftrag>();

		for (Auftrag a : auftragList.values()) {
			if(!a.isCompleted()) {
				tmp.add(a);
			}
			
		}

		return tmp;
	}
	
	public List<Auftrag> getCompletedAuftragList() {
		List<Auftrag> tmp = new ArrayList<Auftrag>();

		for (Auftrag a : auftragList.values()) {
			if(a.isCompleted()) {
				tmp.add(a);
			}
			
		}

		return tmp;
	}

}
