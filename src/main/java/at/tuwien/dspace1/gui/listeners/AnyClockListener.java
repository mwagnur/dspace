package at.tuwien.dspace1.gui.listeners;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.Entry;
import org.mozartspaces.notifications.Notification;
import org.mozartspaces.notifications.NotificationListener;
import org.mozartspaces.notifications.Operation;

import at.tuwien.dspace1.parts.*;
import at.tuwien.dspace1.shared.Util;

public class AnyClockListener implements NotificationListener {
	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference anyRef;

	private long uhren = 0;
	HashMap<Long, Uhr> clockList = new HashMap<Long, Uhr>();

	public AnyClockListener(String containerName) {
		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

			try {
				this.anyRef = Util.getOrCreateNamedContainer(spaceURI,
						containerName, capi);
			} catch (MzsCoreException e) {
				e.printStackTrace();
			}

		try {

			ArrayList<Serializable> tmp = capi.read(anyRef, TypeCoordinator
					.newSelector(Uhr.class, MzsConstants.Selecting.COUNT_MAX),
					MzsConstants.RequestTimeout.ZERO, null);

			for (Serializable s : tmp) {
				Uhr u = (Uhr) s;
				this.uhren++;
				clockList.put(u.getSerialNo(), u);
			}

			if (Util.debugMode()) {
				System.err.println("Initial data: Uhren: " + this.uhren);
			}

		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}
	
	public AnyClockListener(String containerName, Capi capi, MzsCore core, URI spaceURI) {
		this.core = core;
		this.capi = capi;
		this.spaceURI = spaceURI;
		
			try {
				this.anyRef = Util.getOrCreateNamedContainer(spaceURI,
						containerName, capi);
			} catch (MzsCoreException e) {
				e.printStackTrace();
			}

		try {

			ArrayList<Serializable> tmp = capi.read(anyRef, TypeCoordinator
					.newSelector(Uhr.class, MzsConstants.Selecting.COUNT_MAX),
					MzsConstants.RequestTimeout.ZERO, null);

			for (Serializable s : tmp) {
				Uhr u = (Uhr) s;
				this.uhren++;
				clockList.put(u.getSerialNo(), u);
			}

			if (Util.debugMode()) {
				System.err.println("Initial data: Uhren: " + this.uhren);
			}

		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void entryOperationFinished(Notification arg0, Operation arg1,
			List<? extends Serializable> arg2) {
		for (Serializable s : arg2) {

			switch (arg1) {
			case DELETE:
				if (s instanceof Uhr) {
					Uhr u = (Uhr) s;
					clockList.remove(u.getSerialNo());
					this.uhren--;
				}
				break;
			case READ:
				break;
			case TAKE:
				if (s instanceof Uhr) {
					Uhr u = (Uhr) s;
					clockList.remove(u.getSerialNo());
					this.uhren--;
				}
				break;
			case TEST:
				break;
			case WRITE:
				Entry e = null;
				if (s instanceof Entry) {
					e = (Entry) s;
				}
				if ((e != null) && e.getValue() instanceof Uhr) {
					Uhr u = (Uhr) e.getValue();
					clockList.put(u.getSerialNo(), u);
					this.uhren++;
				}
				break;
			default:
				break;
			}
		}
	}

	public long getUhren() {
		return this.uhren;
	}

	public List<Uhr> getClockList() {
		List<Uhr> tmp = new ArrayList<Uhr>();

		for (Uhr u : clockList.values()) {
			tmp.add(u);
		}

		return tmp;
	}

}
