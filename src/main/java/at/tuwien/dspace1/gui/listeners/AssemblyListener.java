package at.tuwien.dspace1.gui.listeners;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.Entry;
import org.mozartspaces.notifications.Notification;
import org.mozartspaces.notifications.NotificationListener;
import org.mozartspaces.notifications.Operation;

import at.tuwien.dspace1.parts.*;
import at.tuwien.dspace1.shared.Util;

public class AssemblyListener implements NotificationListener {
	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference assemblyRef;

	private long gehause = 0;
	private long uhrwerk = 0;
	private long zeiger = 0;
	private long metallArmband = 0;
	private long lederArmband = 0;

	public AssemblyListener() {
		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

			try {
				this.assemblyRef = Util.getOrCreateNamedContainer(spaceURI,
						Util.ASSEMBLY_CONTAINER, capi);
			} catch (MzsCoreException e) {
				e.printStackTrace();
			}

		try {
			this.gehause = capi.test(assemblyRef, TypeCoordinator.newSelector(
					Gehause.class, MzsConstants.Selecting.COUNT_MAX),
					MzsConstants.RequestTimeout.ZERO, null);
			this.uhrwerk = capi.test(assemblyRef, TypeCoordinator.newSelector(
					Uhrwerk.class, MzsConstants.Selecting.COUNT_MAX),
					MzsConstants.RequestTimeout.ZERO, null);
			this.zeiger = capi.test(assemblyRef, TypeCoordinator.newSelector(
					Zeiger.class, MzsConstants.Selecting.COUNT_MAX),
					MzsConstants.RequestTimeout.ZERO, null);
			this.lederArmband = capi.test(assemblyRef, TypeCoordinator.newSelector(
					LederArmband.class, MzsConstants.Selecting.COUNT_MAX),
					MzsConstants.RequestTimeout.ZERO, null);
			this.metallArmband = capi.test(assemblyRef, TypeCoordinator.newSelector(
					MetallArmband.class, MzsConstants.Selecting.COUNT_MAX),
					MzsConstants.RequestTimeout.ZERO, null);

			if (Util.debugMode()) {
				System.err.println("Initial data: Gehause: " + this.gehause
						+ " Uhrwerk: " + this.uhrwerk + " Zeiger: "
						+ this.zeiger + " LederArmband: " + this.lederArmband
						+ " MetallArmband: " + this.metallArmband);
			}

		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void entryOperationFinished(Notification arg0, Operation arg1,
			List<? extends Serializable> arg2) {
		for (Serializable s : arg2) {
			Bestandteil b = null;
			switch (arg1) {
			case DELETE:
				if (s instanceof Bestandteil) {
					b = (Bestandteil) s;
				}
				break;
			case READ:
				break;
			case TAKE:
				if (s instanceof Bestandteil) {
					b = (Bestandteil) s;
				}
				break;
			case TEST:
				break;
			case WRITE:
				Entry e = null;
				if (s instanceof Entry) {
					e = (Entry) s;
				}
				if ((e != null) && (e.getValue() instanceof Bestandteil)) {
					b = (Bestandteil) e.getValue();
				}
				break;
			default:
				break;
			}

			if (b != null) {
				updateCount(arg1, b.getTyp());
			}
		}
	}

	private void updateCount(Operation op, Bestandteil.BestandteilTyp typ) {
		switch (op) {
		case DELETE:
			decrementType(typ);
			break;
		case READ:
			break;
		case TAKE:
			decrementType(typ);
			break;
		case TEST:
			break;
		case WRITE:
			incrementType(typ);
			break;
		default:
			break;
		}
	}

	private void decrementType(Bestandteil.BestandteilTyp typ) {
		switch (typ) {
		case LEDERARMBAND:
			this.lederArmband--;
			break;
		case METALLARMBAND:
			this.metallArmband--;
			break;
		case GEHAUSE:
			this.gehause--;
			break;
		case UHRWERK:
			this.uhrwerk--;
			break;
		case ZEIGER:
			this.zeiger--;
			break;
		default:
			break;
		}
	}

	private void incrementType(Bestandteil.BestandteilTyp typ) {
		switch (typ) {
		case LEDERARMBAND:
			this.lederArmband++;
			break;
		case METALLARMBAND:
			this.metallArmband++;
			break;
		case GEHAUSE:
			this.gehause++;
			break;
		case UHRWERK:
			this.uhrwerk++;
			break;
		case ZEIGER:
			this.zeiger++;
			break;
		default:
			break;
		}
	}

	public long getLederArmband() {
		return this.lederArmband;
	}
	
	public long getMetallArmband() {
		return this.metallArmband;
	}

	public long getZeiger() {
		return this.zeiger;
	}

	public long getUhrwerk() {
		return this.uhrwerk;
	}

	public long getGehause() {
		return this.gehause;
	}

}
