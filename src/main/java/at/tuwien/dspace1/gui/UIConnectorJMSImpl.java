package at.tuwien.dspace1.gui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import at.tuwien.dspace1.jms.JMSUtil;
import at.tuwien.dspace1.jms.LieferantJMS;
import at.tuwien.dspace1.jms.dto.InformationDTO;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.ILieferant;
import at.tuwien.dspace1.shared.Auftrag.Priority;

public class UIConnectorJMSImpl implements UIConnector, MessageListener {

	private MessageConsumer subscriberTopic;
	private MessageProducer benchmarkTopicProducer;
	private MessageProducer senderServer;

	private Connection connection;
	private static Session session;

	private long lederArmbandCount;
	private long metallArmbandCount;
	private long zeigerCount;
	private long uhrwerkCount;
	private long gehauseCount;

	private long qualityClockCount;
	private long logistikClockCount;
	private long finalClockCount;
	private long trashClockCount;

	private List<Uhr> qualityClockList = new ArrayList<Uhr>();
	private List<Uhr> logistikClockList = new ArrayList<Uhr>();
	private List<Uhr> finalClockList = new ArrayList<Uhr>();
	private List<Uhr> trashClockList = new ArrayList<Uhr>();

	private List<Auftrag> activeAuftragList = new ArrayList<Auftrag>();
	private List<Auftrag> completedAuftragList = new ArrayList<Auftrag>();

	private int auftragsId = 0;

	@Override
	public void init() {
		try {
			Context jndiContext = new InitialContext();

			ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext
					.lookup(JMSUtil.CONNECTION_FACTORY);

			connection = connectionFactory.createConnection();
			connection.setClientID(UUID.randomUUID().toString());

			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Topic uiTopicQueue;
			uiTopicQueue = (Topic) jndiContext.lookup(JMSUtil.UI_TOPIC_QUEUE);

			subscriberTopic = session.createDurableSubscriber(uiTopicQueue,
					"gui");
			subscriberTopic.setMessageListener(this);

			Queue serverQueue = (Queue) jndiContext
					.lookup(JMSUtil.SERVER_QUEUE);
			senderServer = session.createProducer(serverQueue);

			Topic clusterTopic = (Topic) jndiContext
					.lookup(JMSUtil.BENCHMARK_TOPIC_QUEUE);
			benchmarkTopicProducer = session.createProducer(clusterTopic);

			connection.start();

			System.out.println("JMS in UIConnector initialized");
		} catch (JMSException e1) {
			e1.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(Message message) {
		ObjectMessage om = (ObjectMessage) message;
		try {
			Serializable object = om.getObject();

			System.out.println("Received topic update!");

			if (object instanceof InformationDTO) {

				InformationDTO info = (InformationDTO) object;

				lederArmbandCount = info.getLederArmbandCount();
				metallArmbandCount = info.getMetallArmbandCount();
				zeigerCount = info.getZeigerCount();
				uhrwerkCount = info.getUhrwerkCount();
				gehauseCount = info.getGehauseCount();

				qualityClockCount = info.getQualityClockCount();
				qualityClockList = info.getQualityClockList();

				logistikClockCount = info.getLogistikClockCount();
				logistikClockList = info.getLogistikClockList();

				finalClockCount = info.getFinalClockCount();
				finalClockList = info.getFinalClockList();

				trashClockCount = info.getTrashClockCount();
				trashClockList = info.getTrashClockList();

				activeAuftragList = info.getActiveAuftragList();
				completedAuftragList = info.getCompletedAuftragList();

				// System.out.println(info);

			} else {
				System.out.println("Unexpected message obj: " + object);
			}

		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

	@Override
	public ILieferant createLieferant(BestandteilTyp typ, int anzahl) {
		return new LieferantJMS(typ, anzahl);
	}

	/* getters for assembly */
	@Override
	public long getLederArmband() {
		return this.lederArmbandCount;
	}

	@Override
	public long getMetallArmband() {
		return this.metallArmbandCount;
	}

	@Override
	public long getZeiger() {
		return zeigerCount;
	}

	@Override
	public long getUhrwerk() {
		return uhrwerkCount;
	}

	@Override
	public long getGehause() {
		return gehauseCount;
	}

	@Override
	public long getQualityUhren() {
		return qualityClockCount;
	}

	@Override
	public long getLogisticUhren() {
		return logistikClockCount;
	}

	@Override
	public long getFinalUhren() {
		return finalClockCount;
	}

	@Override
	public long getTrashUhren() {
		return trashClockCount;
	}

	@Override
	public List<Uhr> getQualityClockList() {
		return qualityClockList;
	}

	@Override
	public List<Uhr> getLogisticClockList() {
		return logistikClockList;
	}

	@Override
	public List<Uhr> getFinalClockList() {
		return finalClockList;
	}

	@Override
	public List<Uhr> getTrashClockList() {
		return trashClockList;
	}

	@Override
	public void startWorkers() {
		try {
			ObjectMessage message = session.createObjectMessage();

			message.setObject(JMSUtil.START_BENCHMARK);

			benchmarkTopicProducer.send(message);
			System.out.println("Publishing START_BENCHMARK to workers");

		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void stopWorkers() {
		try {
			ObjectMessage message = session.createObjectMessage();

			message.setObject(JMSUtil.STOP_BENCHMARK);

			benchmarkTopicProducer.send(message);
			System.out.println("Publishing STOP_BENCHMARK to workers");

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendContract(Priority priority, int klassische, int normale,
			int zweiZonen) {
		Auftrag a = new Auftrag(auftragsId, priority, klassische, normale,
				zweiZonen);

		auftragsId++;

		try {
			ObjectMessage message = session.createObjectMessage();

			message.setObject(a);
			message.setStringProperty("Priority", priority.toString());

			senderServer.send(message);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Auftrag> getActiveAuftragList() {
		return activeAuftragList;
	}

	@Override
	public List<Auftrag> getCompletedAuftragList() {
		return completedAuftragList;
	}

	@Override
	public void prepareBenchmark() {

		
		try {
			ObjectMessage message = session.createObjectMessage();

			message.setObject(JMSUtil.PREPARE_BENCHMARK_MESSAGE);

			senderServer.send(message);
		} catch (JMSException e) {

			e.printStackTrace();
		}

	}

}
