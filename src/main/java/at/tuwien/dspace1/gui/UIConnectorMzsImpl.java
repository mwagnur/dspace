package at.tuwien.dspace1.gui;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import org.mozartspaces.capi3.CountNotMetException;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.TransactionReference;
import org.mozartspaces.core.MzsConstants.RequestTimeout;
import org.mozartspaces.notifications.NotificationManager;
import org.mozartspaces.notifications.Operation;

import at.tuwien.dspace1.gui.listeners.*;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.Auftrag.Priority;
import at.tuwien.dspace1.shared.ILieferant;
import at.tuwien.dspace1.shared.Util;
import at.tuwien.dspace1.space.LieferantMZS;

public class UIConnectorMzsImpl implements UIConnector {

	// XVSM
	private Capi capi;
	private MzsCore core;
	private URI spaceURI;

	private ContainerReference assemblyRef;
	private ContainerReference qualityRef;
	private ContainerReference logisticRef;
	private ContainerReference finalRef;
	private ContainerReference trashRef;
	private ContainerReference benchmarkRef;
	private ContainerReference auftragRef;

	private AssemblyListener assemblyListener = null;
	private AnyClockListener qualityListener = null;
	private AnyClockListener logisticListener = null;
	private AnyClockListener finalListener = null;
	private AnyClockListener trashListener = null;
	private AnyAuftragListener auftragListener = null;

	// private ListView<String> completeSpace = new ListView<String>();
	// private ObservableList<String> spaceObservableList = FXCollections
	// .observableArrayList();

	@Override
	public void init() {
		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

			try {
				this.assemblyRef = Util.getOrCreateNamedContainer(spaceURI,
						Util.ASSEMBLY_CONTAINER, capi);
				this.qualityRef = Util.getOrCreateNamedContainer(spaceURI,
						Util.QUALITY_CONTAINER, capi);
				this.logisticRef = Util.getOrCreateNamedContainer(spaceURI,
						Util.LOGISTIC_CONTAINER, capi);
				this.finalRef = Util.getOrCreateNamedContainer(spaceURI,
						Util.FINAL_CONTAINER, capi);
				this.trashRef = Util.getOrCreateNamedContainer(spaceURI,
						Util.TRASH_CONTAINER, capi);
				this.benchmarkRef = Util.getOrCreateNamedContainer(spaceURI,
						Util.BENCHMARK_CONTAINER, capi);
				this.auftragRef = Util.getOrCreateNamedContainer(spaceURI,
						Util.AUFTRAG_CONTAINER, capi);
			} catch (MzsCoreException e) {
				e.printStackTrace();
			}

		this.setupNotificationListeners();
	}

	public void setupNotificationListeners() {
		assemblyListener = new AssemblyListener();
		qualityListener = new AnyClockListener(Util.QUALITY_CONTAINER);
		logisticListener = new AnyClockListener(Util.LOGISTIC_CONTAINER);
		finalListener = new AnyClockListener(Util.FINAL_CONTAINER);
		trashListener = new AnyClockListener(Util.TRASH_CONTAINER);
		auftragListener = new AnyAuftragListener(Util.AUFTRAG_CONTAINER);

		NotificationManager notificationManager = new NotificationManager(core);

		try {
			notificationManager.createNotification(assemblyRef,
					assemblyListener, Operation.WRITE, Operation.TAKE,
					Operation.DELETE);
			notificationManager.createNotification(qualityRef, qualityListener,
					Operation.WRITE, Operation.TAKE, Operation.DELETE);
			notificationManager.createNotification(logisticRef,
					logisticListener, Operation.WRITE, Operation.TAKE,
					Operation.DELETE);
			notificationManager.createNotification(finalRef, finalListener,
					Operation.WRITE, Operation.TAKE, Operation.DELETE);
			notificationManager.createNotification(trashRef, trashListener,
					Operation.WRITE, Operation.TAKE, Operation.DELETE);
			notificationManager.createNotification(auftragRef, auftragListener,
					Operation.WRITE, Operation.TAKE, Operation.DELETE);
		} catch (MzsCoreException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ILieferant createLieferant(BestandteilTyp typ, int anzahl) {

		return new LieferantMZS(typ, anzahl);
	}

	/* getters for assembly */
	@Override
	public long getLederArmband() {
		if (assemblyListener == null) {
			System.err.println("ASSEMBLY = NULL");
			return 0;
		}
		return assemblyListener.getLederArmband();
	}
	
	@Override
	public long getMetallArmband() {
		if (assemblyListener == null) {
			System.err.println("ASSEMBLY = NULL");
			return 0;
		}
		return assemblyListener.getMetallArmband();
	}

	@Override
	public long getZeiger() {
		if (assemblyListener == null) {
			return 0;
		}
		return assemblyListener.getZeiger();
	}

	@Override
	public long getUhrwerk() {
		if (assemblyListener == null) {
			return 0;
		}
		return assemblyListener.getUhrwerk();
	}

	@Override
	public long getGehause() {
		if (assemblyListener == null) {
			return 0;
		}
		return assemblyListener.getGehause();
	}

	@Override
	public long getQualityUhren() {
		if (qualityListener == null) {
			return 0;
		}
		return qualityListener.getUhren();
	}

	@Override
	public long getLogisticUhren() {
		if (logisticListener == null) {
			return 0;
		}
		return logisticListener.getUhren();
	}

	@Override
	public long getFinalUhren() {
		if (finalListener == null) {
			return 0;
		}
		return finalListener.getUhren();
	}
	
	@Override
	public long getTrashUhren() {
		if (trashListener == null) {
			return 0;
		}
		return trashListener.getUhren();
	}
	

	@Override
	public List<Auftrag> getActiveAuftragList() {
		if (auftragListener == null) {
			return null;
		}
		return auftragListener.getActiveAuftragList();
	}

	@Override
	public List<Auftrag> getCompletedAuftragList() {
		if (auftragListener == null) {
			return null;
		}
		return auftragListener.getCompletedAuftragList();
	}

	@Override
	public List<Uhr> getQualityClockList() {
		if (qualityListener == null) {
			return null;
		}
		return qualityListener.getClockList();
	}

	@Override
	public List<Uhr> getLogisticClockList() {
		if (logisticListener == null) {
			return null;
		}
		return logisticListener.getClockList();
	}

	@Override
	public List<Uhr> getFinalClockList() {
		if (finalListener == null) {
			return null;
		}
		return finalListener.getClockList();
	}
	
	@Override
	public List<Uhr> getTrashClockList() {
		if (trashListener == null) {
			return null;
		}
		return trashListener.getClockList();
	}

	@Override
	public void startWorkers() {
		boolean b = true;
		Entry entry = new Entry(b, TypeCoordinator.newCoordinationData());
		try {
			capi.write(benchmarkRef, RequestTimeout.TRY_ONCE, null, entry);
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stopWorkers() {
		try {
			capi.take(benchmarkRef,
					TypeCoordinator.newSelector(Boolean.class, 1),
					MzsConstants.RequestTimeout.TRY_ONCE, null);
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendContract(Priority priority, int klassische, int normale, int zweiZonen) {
		// TODO mpranj fix
		TransactionReference tx;
		try {
			tx = capi.createTransaction(10000, spaceURI);
			try {
				int auftragId = nextContractNo(tx);
				Auftrag a = new Auftrag(auftragId, priority, klassische, normale, zweiZonen);
				
				Entry entry = new Entry(a,
						TypeCoordinator.newCoordinationData(),
						QueryCoordinator.newCoordinationData());
				
				capi.write(auftragRef, RequestTimeout.TRY_ONCE, tx, entry);
				
				capi.commitTransaction(tx);
			} catch (MzsCoreException ex) {
				capi.rollbackTransaction(tx);
				ex.printStackTrace();
			}
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}
	
	private int nextContractNo(TransactionReference tx) throws MzsCoreException {
		int id = 1;
		try {
			ArrayList<Serializable> resultentries = capi.take(auftragRef,
					TypeCoordinator.newSelector(Integer.class, 1),
					MzsConstants.RequestTimeout.TRY_ONCE, tx);
			try {
				id = (Integer) resultentries.get(0);
			} catch (Exception e) {
			}
			++id;
		} catch (CountNotMetException e) {
			/* new entry created below */
		}
		Entry entry = new Entry(id, TypeCoordinator.newCoordinationData());
		capi.write(auftragRef, RequestTimeout.TRY_ONCE, tx, entry);

		return id;
	}

	@Override
	public void prepareBenchmark() {
		// TODO Auto-generated method stub
		
	}


}
