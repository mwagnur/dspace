package at.tuwien.dspace1.gui;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.notifications.NotificationManager;
import org.mozartspaces.notifications.Operation;

import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.ILieferant;
import at.tuwien.dspace1.shared.Util;
import at.tuwien.dspace1.space.LieferantMZS;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class UhrenfabrikUI extends Application {

	private static UIConnector uiConnector;
	private static BorderPane border = new BorderPane();
	private static ExecutorService threadPool =
			Executors.newCachedThreadPool();


	public UhrenfabrikUI() {

	}

	public static void main(String[] args) {

		if (args == null || !(args.length > 0)) {
			System.err.println("no starting arguments; use either MZS or JMS");
			return;
		} else if (args[0].equalsIgnoreCase("mzs")) {

			System.out.println("started GUI for MozartSpaces");
			uiConnector = new UIConnectorMzsImpl();
			uiConnector.init();
		} else if (args[0].equalsIgnoreCase("jms")) {
			System.out.println("started GUI for JMS");
			uiConnector = new UIConnectorJMSImpl();
			uiConnector.init();
		} else {
			System.err
					.println("Unknown starting argument, use either MZS or JMS");
			return;
		}

		launch(args);
	}
	
	public void newLieferandThread(final BestandteilTyp typ, final int count) {
		Task<Void> task = new Task<Void>() {
			@Override
			public Void call() {
				ILieferant lief = uiConnector
						.createLieferant(typ, count);
				lief.processLieferung();
				return null;
			}
		};
		threadPool.submit(task);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Uhrenfabrik GUI");
		
		/* RIGHT PANE */
		GridPane grid_right = new GridPane();
		grid_right.setAlignment(Pos.CENTER);
		grid_right.setHgap(10);
		grid_right.setVgap(10);
		border.setRight(grid_right);
		
		int r = 0;

		Label klassische_lbl = new Label("Klassische Uhren:");
		grid_right.add(klassische_lbl, 0, ++r);
		
		final TextField klassischeCount = new TextField();
		grid_right.add(klassischeCount, 1, r);
		
		Label normal_lbl = new Label("Normale Sport-Uhren:");
		grid_right.add(normal_lbl, 0, ++r);
		
		final TextField normalCount = new TextField();
		grid_right.add(normalCount, 1, r);
		
		Label twoZone_lbl = new Label("Zwei-Zonen Sport-Uhren:");
		grid_right.add(twoZone_lbl, 0, ++r);
		
		final TextField twoZoneCount = new TextField();
		grid_right.add(twoZoneCount, 1, r);
		
		Label prio_lbl = new Label("Priorität:");
		grid_right.add(prio_lbl, 0, ++r);
		
		ObservableList<String> options_prio = FXCollections.observableArrayList(
				Auftrag.Priority.LOW.name(),
				Auftrag.Priority.NORMAL.name(),
				Auftrag.Priority.HIGH.name());
		final ComboBox comboBox_prio = new ComboBox(options_prio);
		grid_right.add(comboBox_prio, 1, r);
		
		Button contract_btn = new Button("Auftrag senden");
		HBox contract_hbBtn = new HBox(10);
		contract_hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		contract_hbBtn.getChildren().add(contract_btn);
		grid_right.add(contract_hbBtn, 1, ++r);
		
		contract_btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				final Auftrag.Priority prio = Auftrag.Priority.valueOf(
						comboBox_prio.getSelectionModel()
						.getSelectedItem().toString());
				
				final int klassische = Integer.parseInt(klassischeCount.getText());
				final int normale = Integer.parseInt(normalCount.getText());
				final int zweiZonen = Integer.parseInt(twoZoneCount.getText());
				
				Task<Void> task = new Task<Void>() {
					@Override
					public Void call() {
						uiConnector.sendContract(prio, klassische, normale, zweiZonen);
						return null;
					}
				};
				threadPool.submit(task);
			}
		});
		
		VBox centerBox_right = new VBox();
		Label active_contract_lbl = new Label("Aktive Aufträge::");
		Label completed_contract_lbl = new Label("Fertige Aufträge:");

		final ObservableList<String> activeContractList = FXCollections
				.observableArrayList();
		ListView<String> activeContractListView = new ListView<String>();
		activeContractListView.setItems(activeContractList);

		final ObservableList<String> completedContractList = FXCollections
				.observableArrayList();
		ListView<String> completedContractListView = new ListView<String>();
		completedContractListView.setItems(completedContractList);
		
		
		/* END RIGHT PANE */
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		border.setLeft(grid);

		// l is the current line in the layout
		int l = 0;
		
		Button bench_btn = new Button("Prepare Benchmark");
		HBox bench_hbBtn = new HBox(10);
		bench_hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		bench_hbBtn.getChildren().add(bench_btn);
		grid.add(bench_hbBtn, 1, ++l);

		bench_btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				newLieferandThread(BestandteilTyp.GEHAUSE, 1500);
				newLieferandThread(BestandteilTyp.UHRWERK, 1500);
				newLieferandThread(BestandteilTyp.LEDERARMBAND, 750);
				newLieferandThread(BestandteilTyp.METALLARMBAND, 750);
				newLieferandThread(BestandteilTyp.ZEIGER, 3750);
			}
		});
		
		Button start_bench_btn = new Button("Start Benchmark");
		HBox start_bench_hbBtn = new HBox(10);
		start_bench_hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		start_bench_hbBtn.getChildren().add(start_bench_btn);
		grid.add(start_bench_hbBtn, 1, ++l);

		start_bench_btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Task<Void> task = new Task<Void>() {
					@Override
					public Void call() {
						uiConnector.startWorkers();
						try {
							Thread.sleep(60000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						uiConnector.stopWorkers();
						return null;
					}
				};
				threadPool.submit(task);
			}
		});
		
		Button start_workers_btn = new Button("Start Workers");
		HBox start_workers_hbBtn = new HBox(10);
		start_workers_hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		start_workers_hbBtn.getChildren().add(start_workers_btn);
		grid.add(start_workers_hbBtn, 1, ++l);

		start_workers_btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Task<Void> task = new Task<Void>() {
					@Override
					public Void call() {
						uiConnector.startWorkers();
						return null;
					}
				};
				threadPool.submit(task);
			}
		});
		
		Button stop_workers_btn = new Button("Stop Workers");
		HBox stop_workers_hbBtn = new HBox(10);
		stop_workers_hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		stop_workers_hbBtn.getChildren().add(stop_workers_btn);
		grid.add(stop_workers_hbBtn, 1, ++l);

		stop_workers_btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Task<Void> task = new Task<Void>() {
					@Override
					public Void call() {
						uiConnector.stopWorkers();
						return null;
					}
				};
				threadPool.submit(task);
			}
		});
		

		Scene scene = new Scene(border, 400, 600);
		primaryStage.setScene(scene);

		Label userName = new Label("Bestandteil:");
		grid.add(userName, 0, ++l);

		ObservableList<String> options = FXCollections.observableArrayList(
				BestandteilTyp.GEHAUSE.name(), BestandteilTyp.UHRWERK.name(),
				BestandteilTyp.ZEIGER.name(), BestandteilTyp.LEDERARMBAND.name(),
				BestandteilTyp.METALLARMBAND.name());
		final ComboBox comboBox = new ComboBox(options);
		grid.add(comboBox, 1, l);

		Label pw = new Label("Teilanzahl:");
		grid.add(pw, 0, ++l);

		final TextField partCount = new TextField();
		grid.add(partCount, 1, l);

		Label lief = new Label("LieferantenAnz.:");
		grid.add(lief, 0, ++l);

		final TextField workerCount = new TextField();
		grid.add(workerCount, 1, l);

		Button btn = new Button("Sign in");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, ++l);

		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				System.out.println("button pressed: " + partCount.getText()
						+ ", "
						+ comboBox.getSelectionModel().getSelectedIndex());
				// yo dawg, we heard u liek threads
				// so we put threads in your threads
				final BestandteilTyp typ = BestandteilTyp.valueOf(comboBox
						.getSelectionModel().getSelectedItem().toString());
				final int numParts = Integer.parseInt(partCount.getText());
				final int numWorkers = Integer.parseInt(workerCount.getText());
				
				for (int i = 0; i < numWorkers; ++i) {
					Task<Void> task = new Task<Void>() {
						@Override
						public Void call() {
							ILieferant lief = uiConnector
									.createLieferant(typ, numParts);
							lief.processLieferung();
							return null;
						}
					};
					threadPool.submit(task);
				}
			}
		});

		Label assembly_all_lbl = new Label("Assembly ALL:");
		grid.add(assembly_all_lbl, 0, ++l);
		final Label assembly_all_text = new Label("");
		grid.add(assembly_all_text, 1, l);

		Label assembly_g_lbl = new Label("Assembly Gehause:");
		grid.add(assembly_g_lbl, 0, ++l);
		final Label assembly_g_text = new Label("");
		grid.add(assembly_g_text, 1, l);

		Label assembly_u_lbl = new Label("Assembly Uhrwerk:");
		grid.add(assembly_u_lbl, 0, ++l);
		final Label assembly_u_text = new Label("");
		grid.add(assembly_u_text, 1, l);

		Label assembly_z_lbl = new Label("Assembly Zeiger:");
		grid.add(assembly_z_lbl, 0, ++l);
		final Label assembly_z_text = new Label("");
		grid.add(assembly_z_text, 1, l);

		Label assembly_la_lbl = new Label("Assembly LederArmband:");
		grid.add(assembly_la_lbl, 0, ++l);
		final Label assembly_la_text = new Label("");
		grid.add(assembly_la_text, 1, l);
		
		Label assembly_ma_lbl = new Label("Assembly MetallArmband:");
		grid.add(assembly_ma_lbl, 0, ++l);
		final Label assembly_ma_text = new Label("");
		grid.add(assembly_ma_text, 1, l);

		Label quality_all_lbl = new Label("Quality ALL:");
		grid.add(quality_all_lbl, 0, ++l);
		final Label quality_all_text = new Label("");
		grid.add(quality_all_text, 1, l);

		Label logistic_all_lbl = new Label("Logistic ALL:");
		grid.add(logistic_all_lbl, 0, ++l);
		final Label logistic_all_text = new Label("");
		grid.add(logistic_all_text, 1, l);

		Label final_all_lbl = new Label("Final ALL:");
		grid.add(final_all_lbl, 0, ++l);
		final Label final_all_text = new Label("");
		grid.add(final_all_text, 1, l);
		
		Label trash_all_lbl = new Label("Trash ALL:");
		grid.add(trash_all_lbl, 0, ++l);
		final Label trash_all_text = new Label("");
		grid.add(trash_all_text, 1, l);

		/* start task for UI updates */
		new Thread() {
			/* separate thread work */
			public void run() {
				while (true) {
					// System.out.println("UPDATE THREAD ...");
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					/* get information here */
					final long assembly_gehause = uiConnector.getGehause();
					final long assembly_uhrwerk = uiConnector.getUhrwerk();
					final long assembly_zeiger = uiConnector.getZeiger();
					final long assembly_lederArmband = uiConnector.getLederArmband();
					final long assembly_metallArmband = uiConnector.getMetallArmband();
					final long assembly_all = assembly_gehause
							+ assembly_uhrwerk + assembly_zeiger
							+ assembly_lederArmband+ + assembly_metallArmband;

					final long quality_all = uiConnector.getQualityUhren();
					final long logistic_all = uiConnector.getLogisticUhren();
					final long final_all = uiConnector.getFinalUhren();
					final long trash_all = uiConnector.getTrashUhren();
					/* platform.runlater is on UI thread */
					Platform.runLater(new Runnable() {

						public void run() {
							/* update UI elements from here */
							// System.out.println("new count: " + assembly_all);
							assembly_all_text.setText(Long
									.toString(assembly_all));
							assembly_g_text.setText(Long
									.toString(assembly_gehause));
							assembly_u_text.setText(Long
									.toString(assembly_uhrwerk));
							assembly_z_text.setText(Long
									.toString(assembly_zeiger));
							assembly_la_text.setText(Long
									.toString(assembly_lederArmband));
							assembly_ma_text.setText(Long
									.toString(assembly_metallArmband));

							quality_all_text.setText(Long.toString(quality_all));
							logistic_all_text.setText(Long
									.toString(logistic_all));
							final_all_text.setText(Long.toString(final_all));
							trash_all_text.setText(Long.toString(trash_all));
						}
					});
				}

			}
		}.start();

		/* ui list stuff goes here */
		// List<String> qual = new ArrayList<String>();
		VBox centerBox = new VBox();
		Label quality_lbl = new Label("Quality container:");
		Label logistic_lbl = new Label("Logistic container:");
		Label final_lbl = new Label("Final container:");
		Label trash_lbl = new Label("Trash container:");

		final ObservableList<String> qualityList = FXCollections
				.observableArrayList();
		ListView<String> qualityListView = new ListView<String>();
		qualityListView.setItems(qualityList);

		final ObservableList<String> logisticList = FXCollections
				.observableArrayList();
		ListView<String> logisticListView = new ListView<String>();
		logisticListView.setItems(logisticList);

		final ObservableList<String> finalList = FXCollections
				.observableArrayList();
		ListView<String> finalListView = new ListView<String>();
		finalListView.setItems(finalList);

		final ObservableList<String> trashList = FXCollections
				.observableArrayList();
		ListView<String> trashListView = new ListView<String>();
		trashListView.setItems(trashList);

		centerBox.getChildren().addAll(quality_lbl, qualityListView,
				logistic_lbl, logisticListView, final_lbl, finalListView,
				trash_lbl, trashListView,
				active_contract_lbl,
				activeContractListView,
				completed_contract_lbl,
				completedContractListView
				);

		border.setCenter(centerBox);

		new Thread() {
			/* separate thread work */
			public void run() {
				while (true) {
					// System.out.println("UPDATE THREAD ...");
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					/* get information here */
					List<Uhr> qualityClockList = uiConnector
							.getQualityClockList();
					List<Uhr> logisticClockList = uiConnector
							.getLogisticClockList();
					List<Uhr> finalClockList = uiConnector.getFinalClockList();
					List<Uhr> trashClockList = uiConnector.getTrashClockList();
					List<Auftrag> activeAuftragList = uiConnector.getActiveAuftragList();
					List<Auftrag> completedAuftragList = uiConnector.getCompletedAuftragList();

					final List<String> qualityStr = new ArrayList<String>();
					final List<String> logisticStr = new ArrayList<String>();
					final List<String> finalStr = new ArrayList<String>();
					final List<String> trashStr = new ArrayList<String>();
					final List<String> activeStr = new ArrayList<String>();
					final List<String> completedStr = new ArrayList<String>();

					for (Uhr u : qualityClockList) {
						qualityStr.add(u.toString());
					}
					for (Uhr u : logisticClockList) {
						logisticStr.add(u.toString());
					}
					for (Uhr u : finalClockList) {
						finalStr.add(u.toString());
					}
					for (Uhr u : trashClockList) {
						trashStr.add(u.toString());
					}
					for(Auftrag a : activeAuftragList) {
						activeStr.add(a.toString());
					}
					for(Auftrag a : completedAuftragList) {
						completedStr.add(a.toString());
					}

					/* platform.runlater is on UI thread */
					Platform.runLater(new Runnable() {

						public void run() {
							/* update UI elements from here */
							qualityList.clear();
							qualityList.addAll(qualityStr);
							logisticList.clear();
							logisticList.addAll(logisticStr);
							finalList.clear();
							finalList.addAll(finalStr);
							trashList.clear();
							trashList.addAll(trashStr);
							activeContractList.clear();
							activeContractList.addAll(activeStr);
							completedContractList.clear();
							completedContractList.addAll(completedStr);
						}
					});
				}

			}
		}.start();

		primaryStage.show();
	}

}
