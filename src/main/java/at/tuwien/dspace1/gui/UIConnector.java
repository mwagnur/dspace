package at.tuwien.dspace1.gui;

import java.util.List;

import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.ILieferant;
import at.tuwien.dspace1.shared.Auftrag.Priority;

public interface UIConnector {
	public void init();

	public ILieferant createLieferant(BestandteilTyp typ, int anzahl);

	/* benchmark stuff */
	public void startWorkers();
	public void stopWorkers();
	
	/* contract stuff */
	public void sendContract(Priority priority, int klassische,
			int normale, int zweiZonen);
	
	public List<Auftrag> getActiveAuftragList();
	
	public List<Auftrag> getCompletedAuftragList();
	
	/* assembly item count stuff */
	public long getLederArmband();
	
	public long getMetallArmband();

	public long getZeiger();

	public long getUhrwerk();

	public long getGehause();

	/* div. uhren count stuff */
	public long getQualityUhren();

	public long getLogisticUhren();

	public long getFinalUhren();
	
	public long getTrashUhren();

	public List<Uhr> getQualityClockList();

	public List<Uhr> getLogisticClockList();

	public List<Uhr> getFinalClockList();
	
	public List<Uhr> getTrashClockList();
	
	public void prepareBenchmark();
}
