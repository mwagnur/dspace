package at.tuwien.dspace1.gui.grosshaendler;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.mozartspaces.capi3.Coordinator;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.TransactionReference;
import org.mozartspaces.core.MzsConstants.Container;
import org.mozartspaces.core.MzsConstants.RequestTimeout;
import org.mozartspaces.notifications.NotificationManager;
import org.mozartspaces.notifications.Operation;

import at.tuwien.dspace1.gui.listeners.AnyAuftragListener;
import at.tuwien.dspace1.gui.listeners.AnyClockListener;
import at.tuwien.dspace1.gui.listeners.AssemblyListener;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.GrossAuftrag;
import at.tuwien.dspace1.shared.Util;
import at.tuwien.dspace1.shared.Auftrag.Priority;

public class UIConnectorMzsImplGrossHaendler implements UIConnectorGrossHaendler {
	// XVSM
	private Capi fabricCapi;
	private MzsCore fabricCore;
	
	private Capi selfCapi;
	private MzsCore selfCore;
	
	private URI fabricURI;
	private URI selfURI;
	
	private ContainerReference fabricRef;
	private ContainerReference haendlerRef;
	
	private AnyClockListener myClockListener = null;
	
	int klassische, normale, zweiZonen;
	
	
	private static String fabrikName = "";
	//public static final String SITE_URI = "xvsm://localhost:";

	@Override
	public void init(String fabrikURL) {
		fabrikName = fabrikURL;
		if ((fabrikURL == null) || fabrikURL.isEmpty()) {
			fabrikName = "NONAMEADASDASD";
		}
		
		fabricCore = Util.getCore();
		fabricCapi = Util.getCapi();
		fabricURI = Util.getSpaceURI();
		
		selfCore = DefaultMzsCore.newInstance(0);
		selfCapi = new Capi(selfCore);
		selfURI = selfCore.getConfig().getSpaceUri();
		
		System.out.println(">>>>>>> MY OWN URI: " + selfURI.toASCIIString());
		
		try {
			this.fabricRef = Util.getOrCreateNamedContainer(fabricURI,
					Util.HAENDLER_CONTAINER, fabricCapi);
			this.haendlerRef = UIConnectorMzsImplGrossHaendler.getOrCreateNamedContainer(selfURI,
					fabrikName, selfCapi);
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
		
		this.setupNotificationListeners();
		this.sendAuftrag(klassische, normale, zweiZonen);
	}

	public void setupNotificationListeners() {
		myClockListener = new AnyClockListener(fabrikName, selfCapi, selfCore, selfURI);

		NotificationManager notificationManager = new NotificationManager(selfCore);

		try {
			notificationManager.createNotification(haendlerRef,
					myClockListener, Operation.WRITE, Operation.TAKE,
					Operation.DELETE);
		} catch (MzsCoreException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static ContainerReference getOrCreateNamedContainer(URI space,
			String containerName, Capi capi)
					throws MzsCoreException {

				ContainerReference cref;
				try {
					// try to find existing container
					cref = capi.lookupContainer(containerName, space,
							RequestTimeout.DEFAULT, null);
					System.out.println("INFO: found container");
				} catch (MzsCoreException e) {
					// no container found -> create new container
					System.out.println("INFO: container not found, creating new one");

					ArrayList<Coordinator> obligatoryCoords = new ArrayList<Coordinator>();
					obligatoryCoords.add(new TypeCoordinator());
					obligatoryCoords.add(new QueryCoordinator());
					cref = capi.createContainer(containerName, space,
							Container.UNBOUNDED, obligatoryCoords, null, null);
					System.out.println("INFO: container created");
				}

				return cref;
	}
	
	public void sendAuftrag(int klassische, int normale, int zweiZonen) {
		TransactionReference tx;
		try {
			tx = fabricCapi.createTransaction(10000, fabricURI);
			try {
				GrossAuftrag a = new GrossAuftrag(haendlerRef, selfURI, klassische, normale, zweiZonen);
				
				Entry entry = new Entry(a,
						TypeCoordinator.newCoordinationData(),
						QueryCoordinator.newCoordinationData());
				
				fabricCapi.write(fabricRef, RequestTimeout.TRY_ONCE, tx, entry);
				
				fabricCapi.commitTransaction(tx);
			} catch (MzsCoreException ex) {
				fabricCapi.rollbackTransaction(tx);
				ex.printStackTrace();
			}
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Uhr> getLagerList() {
		if (myClockListener == null) {
			return null;
		}
		return myClockListener.getClockList();
	}

	@Override
	public void changeLager(int klassische, int normale, int zweiZonen) {

		this.klassische = klassische;
		this.normale = normale;
		this.zweiZonen = zweiZonen;
		this.sendAuftrag(klassische, normale, zweiZonen);
	}

	@Override
	public int getKlassischeUhrenMax() {

		return this.klassische;
	}

	@Override
	public int getNormaleSportUhrenMax() {

		return this.normale;
	}

	@Override
	public int getZweiZonenSportUhrenMax() {

		return this.zweiZonen;
	}

	@Override
	public void sellClock() {
		// TODO Auto-generated method stub
		
	}

}
