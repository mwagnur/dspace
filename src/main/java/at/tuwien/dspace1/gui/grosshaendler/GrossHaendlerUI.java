package at.tuwien.dspace1.gui.grosshaendler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import at.tuwien.dspace1.gui.UIConnector;
import at.tuwien.dspace1.gui.UIConnectorJMSImpl;
import at.tuwien.dspace1.gui.UIConnectorMzsImpl;
import at.tuwien.dspace1.parts.KlassischeUhr;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.ILieferant;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class GrossHaendlerUI extends Application {

	private static UIConnectorGrossHaendler uiConnector;
	private static ExecutorService threadPool = Executors.newCachedThreadPool();

	private static String fabrikURL;

	public static void main(String[] args) {

		if (args == null || !(args.length > 0) || args.length < 2) {
			System.err
					.println("no starting arguments; use either MZS or JMS + fabrikURL ");
			return;
		} else if (args[0].equalsIgnoreCase("mzs")) {

			System.out.println("started Grosshaendler GUI for MozartSpaces");
			uiConnector = new UIConnectorMzsImplGrossHaendler();
		} else if (args[0].equalsIgnoreCase("jms")) {
			System.out.println("started Grosshaendler GUI for JMS");
			uiConnector = new UIConnectorJMSImplGrossHaendler();

		} else {
			System.err
					.println("Unknown starting argument, use either MZS or JMS");
			return;
		}

		fabrikURL = args[1];
		uiConnector.init(fabrikURL);

		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("GrossHaendler GUI");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);

		int r = 0;

		Label max = new Label("Maximale Anzahl Uhren:");
		grid.add(max, 0, r);

		Label klassische_lbl = new Label("Klassische Uhren:");
		grid.add(klassische_lbl, 0, ++r);

		final TextField klassischeCount = new TextField();
		grid.add(klassischeCount, 1, r);

		Label normal_lbl = new Label("Normale Sport-Uhren:");
		grid.add(normal_lbl, 0, ++r);

		final TextField normalCount = new TextField();
		grid.add(normalCount, 1, r);

		Label twoZone_lbl = new Label("Zwei-Zonen Sport-Uhren:");
		grid.add(twoZone_lbl, 0, ++r);

		final TextField twoZoneCount = new TextField();
		grid.add(twoZoneCount, 1, r);

		Button btn = new Button("Change");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, ++r);

		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				System.out.println("button pressed: "
						+ klassischeCount.getText() + ", "
						+ normalCount.getText() + ", " + twoZoneCount.getText());
				// yo dawg, we heard u liek threads
				// so we put threads in your threads
				final int klassische = Integer.parseInt(klassischeCount
						.getText());
				final int normale = Integer.parseInt(normalCount.getText());
				final int zweiZonen = Integer.parseInt(twoZoneCount.getText());

				Task<Void> task = new Task<Void>() {
					@Override
					public Void call() {
						uiConnector.changeLager(klassische, normale, zweiZonen);
						return null;
					}
				};
				threadPool.submit(task);
			}
		});

		Label clocks_lbl = new Label("Grosshaendler container:");
		final ObservableList<String> clockList = FXCollections
				.observableArrayList();
		ListView<String> clockListView = new ListView<String>();
		clockListView.setItems(clockList);

		grid.add(clocks_lbl, 0, ++r);

		grid.add(clockListView, 0, ++r);

		new Thread() {
			/* separate thread work */
			public void run() {
				while (true) {
					// System.out.println("UPDATE THREAD ...");
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					/* get information here */
					final List<Uhr> lagerClockList = uiConnector.getLagerList();

					final List<String> qualityStr = new ArrayList<String>();

					if (lagerClockList == null) {
						System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>> lagerClockList is null; abort");
						return;
					}
					for (Uhr u : lagerClockList) {
						qualityStr.add(u.toString());
					}

					/* platform.runlater is on UI thread */
					Platform.runLater(new Runnable() {

						public void run() {
							/* update UI elements from here */
							clockList.clear();
							clockList.addAll(qualityStr);

						}
					});
				}

			}
		}.start();

		new Thread() {
			/* separate thread work */
			public void run() {
				while (true) {
					// System.out.println("UPDATE THREAD ...");
					simulateSellingTime();

					uiConnector.sellClock();
					
					System.out.println("tried to sell clock");
				}

			}

			private void simulateSellingTime() {
				Random rand = new Random();
				int time = rand.nextInt(10000);

				time = (time < 5000) ? 5000 : time;

				try {
					Thread.sleep(time);
				} catch (InterruptedException e) {
					System.out.println("I WAS ASLEEP AND YOU INTERRUPTED ME");
				}

			}
		}.start();

		Scene scene = new Scene(grid, 400, 600);
		primaryStage.setScene(scene);

		primaryStage.show();
	}

}
