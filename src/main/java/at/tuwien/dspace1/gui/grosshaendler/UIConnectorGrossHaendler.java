package at.tuwien.dspace1.gui.grosshaendler;

import java.util.List;

import at.tuwien.dspace1.parts.Uhr;

public interface UIConnectorGrossHaendler {

	public void init(String fabrikURL);
	
	public List<Uhr> getLagerList();
	
	public void changeLager(int klassische, int normale, int zweiZonen);
	
	public int getKlassischeUhrenMax();
	public int getNormaleSportUhrenMax();
	public int getZweiZonenSportUhrenMax();
	
	public void sellClock();

}
