package at.tuwien.dspace1.gui.grosshaendler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import at.tuwien.dspace1.jms.JMSUtil;
import at.tuwien.dspace1.jms.dto.GrossHandlerDTO;
import at.tuwien.dspace1.parts.KlassischeUhr;
import at.tuwien.dspace1.parts.NormaleSportUhr;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.ZweiZonenSportUhr;
import at.tuwien.dspace1.shared.Util;

public class UIConnectorJMSImplGrossHaendler implements
		UIConnectorGrossHaendler, MessageListener {
	private MessageProducer senderServer;

	private MessageConsumer receiver;

	private Connection connection;
	private static Session session;

	private Queue fabrikQueue;
	private String fabrikURI;

	private List<Uhr> lagerList = new ArrayList<Uhr>();

	private int klassischeUhrenMax = 0;
	private int normaleSportUhrenMax = 0;
	private int zweiZonenSportUhrenMax = 0;

	private int klassischeUhrenCurrent = 0;
	private int normaleSportUhrenCurrent = 0;
	private int zweiZonenSportUhrenCurrent = 0;

	@Override
	public void init(String fabrikURL) {
		Context jndiContext;
		try {
			jndiContext = new InitialContext();

			ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext
					.lookup(JMSUtil.CONNECTION_FACTORY);

			connection = connectionFactory.createConnection();
			connection.setClientID(UUID.randomUUID().toString());

			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Queue serverQueue = (Queue) jndiContext
					.lookup(JMSUtil.SERVER_QUEUE);
			senderServer = session.createProducer(serverQueue);

			fabrikURI = fabrikURL;
			fabrikQueue = session.createQueue(fabrikURL);
			receiver = session.createConsumer(fabrikQueue);
			receiver.setMessageListener(this);

			sendToServerLagerUpdate();

			connection.start();
		} catch (NamingException e) {

			e.printStackTrace();
		} catch (JMSException e) {

			e.printStackTrace();
		}
	}

	private void sendToServerLagerUpdate() throws JMSException {
		GrossHandlerDTO dto = new GrossHandlerDTO(klassischeUhrenMax,
				normaleSportUhrenMax, zweiZonenSportUhrenMax);
		dto.setHandlerDestination(fabrikQueue);
		dto.setHandlerURI(fabrikURI);

		dto.setKlassischeUhrenCurrent(klassischeUhrenCurrent);
		dto.setNormaleSportUhrenCurrent(normaleSportUhrenCurrent);
		dto.setZweiZonenSportUhrenCurrent(zweiZonenSportUhrenCurrent);

		sendToServer(dto);
	}

	private void sendToServer(Serializable obj) throws JMSException {

		ObjectMessage message = session.createObjectMessage();
		message.setObject(obj);
		message.setJMSReplyTo(fabrikQueue);
		message.getJMSReplyTo();

		senderServer.send(message);
		System.out.println("sent to server: " + obj);
	}

	@Override
	public List<Uhr> getLagerList() {
		return lagerList;
	}

	@Override
	public void changeLager(int klassische, int normale, int zweiZonen) {

		klassischeUhrenMax = klassische;
		normaleSportUhrenMax = normale;
		zweiZonenSportUhrenMax = zweiZonen;

		GrossHandlerDTO dto = new GrossHandlerDTO(klassischeUhrenMax,
				normaleSportUhrenMax, zweiZonenSportUhrenMax);
		dto.setHandlerDestination(fabrikQueue);
		dto.setHandlerURI(fabrikURI);

		try {
			sendToServer(dto);

			System.out.println("send max uhren update to server");

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(Message message) {
		ObjectMessage om = (ObjectMessage) message;
		try {
			Serializable object = om.getObject();
			System.out.println("Received: " + object);

			if (object instanceof Uhr) {
				Uhr uhr = (Uhr) object;
				lagerList.add(uhr);

				if (uhr instanceof KlassischeUhr) {
					klassischeUhrenCurrent++;
				} else if (uhr instanceof NormaleSportUhr) {
					normaleSportUhrenCurrent++;
				} else if (uhr instanceof ZweiZonenSportUhr) {
					zweiZonenSportUhrenCurrent++;
				} else {
					System.err.println("Received wrong Uhr");
				}

				GrossHandlerDTO dto = new GrossHandlerDTO(klassischeUhrenMax,
						normaleSportUhrenMax, zweiZonenSportUhrenMax);
				dto.setHandlerDestination(fabrikQueue);
				dto.setHandlerURI(fabrikURI);
				dto.setKlassischeUhrenCurrent(klassischeUhrenCurrent);
				dto.setNormaleSportUhrenCurrent(normaleSportUhrenCurrent);
				dto.setZweiZonenSportUhrenCurrent(zweiZonenSportUhrenCurrent);

				sendToServer(dto);

				System.out.println("added Uhr to GUI - " + uhr);
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void sellClock() {

		if (lagerList.isEmpty()) {
			System.out.println("Nothing to sell..");
			return;
		}
		Random rand = new Random();
		int sellInt = rand.nextInt(lagerList.size());

		Uhr uhr = lagerList.get(sellInt);

		if (uhr instanceof KlassischeUhr) {
			klassischeUhrenCurrent--;
		} else if (uhr instanceof NormaleSportUhr) {
			normaleSportUhrenCurrent--;
		} else if (uhr instanceof ZweiZonenSportUhr) {
			zweiZonenSportUhrenCurrent--;
		} else {
			System.err.println("wrong Uhr");
		}

		lagerList.remove(sellInt);

		try {
			sendToServerLagerUpdate();
		} catch (JMSException e) {
			e.printStackTrace();
		}

		System.out.println("sold uhr " + uhr);

	}

	@Override
	public int getKlassischeUhrenMax() {
		return klassischeUhrenMax;
	}

	@Override
	public int getNormaleSportUhrenMax() {
		return normaleSportUhrenMax;
	}

	@Override
	public int getZweiZonenSportUhrenMax() {
		return zweiZonenSportUhrenMax;
	}

}
