package at.tuwien.dspace1.shared;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.notifications.NotificationManager;
import org.mozartspaces.notifications.Operation;

import at.tuwien.dspace1.gui.UIConnector;
import at.tuwien.dspace1.gui.UIConnectorJMSImpl;
import at.tuwien.dspace1.gui.UIConnectorMzsImpl;
import at.tuwien.dspace1.gui.listeners.AnyClockListener;
import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.ILieferant;
import at.tuwien.dspace1.shared.Util;
import at.tuwien.dspace1.space.LieferantMZS;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CLIBenchmark {
	// XVSM
	private static Capi capi;
	private static MzsCore core;
	private static URI spaceURI;

	private static ContainerReference finalRef;
	private static ContainerReference trashRef;

	private static AnyClockListener finalListener = null;
	private static AnyClockListener trashListener = null;

	private static UIConnector uiConnector;
	private static ExecutorService threadPool = Executors.newCachedThreadPool();

	public CLIBenchmark() {

	}

	public static void main(String[] args) {

		if (args == null || !(args.length > 0)) {
			System.err.println("no starting arguments; use either MZS or JMS");
			return;
		} else if (args[0].equalsIgnoreCase("mzs")) {

			System.out.println("started CLI for MozartSpaces");
			uiConnector = new UIConnectorMzsImpl();
			uiConnector.init();
			doBenchmarkMZS();
		} else if (args[0].equalsIgnoreCase("jms")) {
			System.out.println("started CLI for JMS");
			uiConnector = new UIConnectorJMSImpl();
			uiConnector.init();
			uiConnector.prepareBenchmark();
			doBenchmarkJMS();
		} else {
			System.err
					.println("Unknown starting argument, use either MZS or JMS");
			return;
		}

		
	}

	public static void blockingLieferung(final BestandteilTyp typ,
			final int count) {
		ILieferant lief = uiConnector.createLieferant(typ, count);
		lief.processLieferung();
	}

	public static void doBenchmarkMZS() {
		core = Util.getCore();
		capi = Util.getCapi();
		spaceURI = Util.getSpaceURI();

		try {
			finalRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.FINAL_CONTAINER, capi);
			trashRef = Util.getOrCreateNamedContainer(spaceURI,
					Util.TRASH_CONTAINER, capi);
		} catch (MzsCoreException e) {
			e.printStackTrace();
		}

		blockingLieferung(BestandteilTyp.GEHAUSE, 1500);
		blockingLieferung(BestandteilTyp.UHRWERK, 1500);
		blockingLieferung(BestandteilTyp.LEDERARMBAND, 750);
		blockingLieferung(BestandteilTyp.METALLARMBAND, 750);
		blockingLieferung(BestandteilTyp.ZEIGER, 3750);

		System.out.println("> Prepared benchmark, starting workers ...");

		uiConnector.startWorkers();
		try {
			Thread.sleep(60000);
		} catch (InterruptedException asdf) {
			asdf.printStackTrace();
		}
		uiConnector.stopWorkers();

		System.out.println("> Benchmark done! Calculating results ...");

		finalListener = new AnyClockListener(Util.FINAL_CONTAINER);
		trashListener = new AnyClockListener(Util.TRASH_CONTAINER);

		long guteUhren = finalListener.getUhren();
		long schlechteUhren = trashListener.getUhren();

		System.out.println("Gute      Uhren: " + guteUhren);
		System.out.println("Schlechte Uhren: " + schlechteUhren);
		System.out.println("Alle      Uhren: " + (guteUhren + schlechteUhren));
		System.exit(0);
	}

	public static void doBenchmarkJMS() {
		System.out.println("> Prepared benchmark, starting workers ...");

		uiConnector.startWorkers();
		try {
			Thread.sleep(60000);
		} catch (InterruptedException asdf) {
			asdf.printStackTrace();
		}
		uiConnector.stopWorkers();

		System.out.println("> Benchmark done! Calculating results ...");

		long guteUhren = uiConnector.getFinalUhren();
		long schlechteUhren = uiConnector.getTrashUhren();

		System.out.println("Gute      Uhren: " + guteUhren);
		System.out.println("Schlechte Uhren: " + schlechteUhren);
		System.out.println("Alle      Uhren: " + (guteUhren + schlechteUhren));
		System.exit(0);
	}
}
