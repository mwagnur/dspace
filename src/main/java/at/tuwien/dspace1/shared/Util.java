package at.tuwien.dspace1.shared;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.mozartspaces.capi3.Coordinator;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.TransactionReference;
import org.mozartspaces.core.MzsConstants.Container;
import org.mozartspaces.core.MzsConstants.RequestTimeout;
import org.mozartspaces.core.MzsCoreException;

import at.tuwien.dspace1.parts.Uhr;

public class Util {
	public static final String SITE_URI = "xvsm://localhost:4242";
	public static final String ASSEMBLY_CONTAINER = "assembly";
	public static final String QUALITY_CONTAINER = "quality";
	public static final String LOGISTIC_CONTAINER = "logistic";
	public static final String FINAL_CONTAINER = "final";
	public static final String TRASH_CONTAINER = "trash";
	public static final String AUFTRAG_CONTAINER = "auftrag";
	public static final String BENCHMARK_CONTAINER = "benchmark";
	public static final String HAENDLER_CONTAINER = "haendler";

	public static final String LABEL_GEHAEUSE = "gehaeuse";
	public static final String LABEL_UHRWERK = "uhrwerk";
	public static final String LABEL_ZEIGER = "zeiger";
	public static final String LABEL_ARMBAND = "armband";
	
	// XVSM
	private static Capi capi = null;
	private static MzsCore core = null;
	private static URI spaceURI = null;
	
	
	public synchronized static Capi getCapi() {
		if (capi == null) {
			if(core == null) {
				getCore();
			}
			capi = new Capi(core);
		}
		return capi;
	}

	public synchronized static MzsCore getCore() {
		if (core == null){
			core = DefaultMzsCore.newInstance(0);
		}
		return core;
	}

	public synchronized static URI getSpaceURI() {
		if (spaceURI == null) {
			try {
				spaceURI = new URI(Util.SITE_URI);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return spaceURI;
	}

	public static ContainerReference getOrCreateNamedContainer(URI space,
	String containerName, Capi capi)
			throws MzsCoreException {

		ContainerReference cref;
		try {
			// try to find existing container
			cref = capi.lookupContainer(containerName, space,
					RequestTimeout.DEFAULT, null);
			System.out.println("INFO: found container");
		} catch (MzsCoreException e) {
			// no container found -> create new container
			System.out.println("INFO: container not found, creating new one");

			ArrayList<Coordinator> obligatoryCoords = new ArrayList<Coordinator>();
			obligatoryCoords.add(new TypeCoordinator());
			obligatoryCoords.add(new QueryCoordinator());
			cref = capi.createContainer(containerName, space,
					Container.UNBOUNDED, obligatoryCoords, null, null);
			System.out.println("INFO: container created");
		}

		return cref;
	}

	public static boolean debugMode() {
		/* TODO: return false before submitting this assignment */
		return true;
	}
	
	public static void writeClock(Uhr u, Capi capi, ContainerReference cref, TransactionReference tx)
			throws MzsCoreException {
		System.out.println("writing like a rocket into the space");

		Entry entry = new Entry(u, 
				TypeCoordinator.newCoordinationData(),
				QueryCoordinator.newCoordinationData());

		capi.write(cref, RequestTimeout.TRY_ONCE, tx, entry);

		System.out.println("apparently written into the space!");
	}
	
	public static void writeAuftrag(Auftrag a, Capi capi, ContainerReference cref, TransactionReference tx)
			throws MzsCoreException {
		System.out.println("writing like a rocket into the space");

		Entry entry = new Entry(a, 
				TypeCoordinator.newCoordinationData(),
				QueryCoordinator.newCoordinationData());

		capi.write(cref, RequestTimeout.TRY_ONCE, tx, entry);

		System.out.println("apparently written into the space!");
	}
	
	public static void writeGrossAuftrag(GrossAuftrag a, Capi capi, ContainerReference cref, TransactionReference tx)
			throws MzsCoreException {
		System.out.println("writing like a rocket into the space");

		Entry entry = new Entry(a, 
				TypeCoordinator.newCoordinationData(),
				QueryCoordinator.newCoordinationData());

		capi.write(cref, RequestTimeout.TRY_ONCE, tx, entry);

		System.out.println("apparently written into the space!");
	}

}
