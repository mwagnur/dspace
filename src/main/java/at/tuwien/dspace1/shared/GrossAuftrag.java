package at.tuwien.dspace1.shared;

import java.io.Serializable;
import java.util.Date;
import java.net.URI;

import org.mozartspaces.core.ContainerReference;

public class GrossAuftrag implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Priority {
		LOW, NORMAL, HIGH
	}

	private int klassischeUhren;
	private int normaleSportUhren;
	private int zweiZonenSportUhren;
	
	private boolean isDone;
	
	private URI spaceURI;
	private ContainerReference cref;

	
	public GrossAuftrag(ContainerReference cref, URI spaceURI, int klassischeUhren, int normaleSportUhren,
			int zweiZonenSportUhren) {
		isDone = false;
		this.cref = cref;
		this.spaceURI = spaceURI;
		this.klassischeUhren = klassischeUhren;
		this.normaleSportUhren = normaleSportUhren;
		this.zweiZonenSportUhren = zweiZonenSportUhren;
	}

	public int getKlassischeUhren() {
		return klassischeUhren;
	}

	public void setKlassischeUhren(int klassischeUhren) {
		this.klassischeUhren = klassischeUhren;
	}

	public int getNormaleSportUhren() {
		return normaleSportUhren;
	}

	public void setNormaleSportUhren(int normaleSportUhren) {
		this.normaleSportUhren = normaleSportUhren;
	}

	public int getZweiZonenSportUhren() {
		return zweiZonenSportUhren;
	}

	public void setZweiZonenSportUhren(int zweiZonenSportUhren) {
		this.zweiZonenSportUhren = zweiZonenSportUhren;
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

	public ContainerReference getCref() {
		return cref;
	}

	public void setCref(ContainerReference cref) {
		this.cref = cref;
	}
}
