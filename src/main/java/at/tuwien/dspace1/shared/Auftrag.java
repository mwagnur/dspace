package at.tuwien.dspace1.shared;

import java.io.Serializable;

public class Auftrag implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Priority {
		LOW, NORMAL, HIGH
	}
	private int id;
	private Priority priority;
	private int klassischeUhren;
	private int normaleSportUhren;
	private int zweiZonenSportUhren;
	private boolean completed;
	
	
	private int klassischeUhrenUnfinished;
	private int normaleSportUhrenUnfinished;
	private int zweiZonenSportUhrenUnfinished;
	
	public Auftrag(int id,  Priority priority, int klassischeUhren, int normaleSportUhren,
			int zweiZonenSportUhren) {
		this.completed = false;
		this.id = id;
		this.priority = priority;
		this.klassischeUhren = klassischeUhren;
		this.normaleSportUhren = normaleSportUhren;
		this.zweiZonenSportUhren = zweiZonenSportUhren;
		this.klassischeUhrenUnfinished = klassischeUhren;
		this.normaleSportUhrenUnfinished = normaleSportUhren;
		this.zweiZonenSportUhrenUnfinished = zweiZonenSportUhren;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getKlassischeUhren() {
		return klassischeUhren;
	}

	public void setKlassischeUhren(int klassischeUhren) {
		this.klassischeUhren = klassischeUhren;
	}

	public int getNormaleSportUhren() {
		return normaleSportUhren;
	}

	public void setNormaleSportUhren(int normaleSportUhren) {
		this.normaleSportUhren = normaleSportUhren;
	}

	public int getZweiZonenSportUhren() {
		return zweiZonenSportUhren;
	}

	public void setZweiZonenSportUhren(int zweiZonenSportUhren) {
		this.zweiZonenSportUhren = zweiZonenSportUhren;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void markCompleted() {
		this.completed = true;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}
	@Override
	public String toString() {
		return "Auftrag [id=" + id + ", completed=" + completed + ", priority="
				+ priority + ", klassischeUhren=" + klassischeUhren
				+ ", normaleSportUhren=" + normaleSportUhren
				+ ", zweiZonenSportUhren=" + zweiZonenSportUhren + "]";
	}

	public int getKlassischeUhrenUnfinished() {
		return klassischeUhrenUnfinished;
	}

	public void setKlassischeUhrenUnfinished(int klassischeUhrenUnfinished) {
		this.klassischeUhrenUnfinished = klassischeUhrenUnfinished;
	}

	public int getNormaleSportUhrenUnfinished() {
		return normaleSportUhrenUnfinished;
	}

	public void setNormaleSportUhrenUnfinished(int normaleSportUhrenUnfinished) {
		this.normaleSportUhrenUnfinished = normaleSportUhrenUnfinished;
	}

	public int getZweiZonenSportUhrenUnfinished() {
		return zweiZonenSportUhrenUnfinished;
	}

	public void setZweiZonenSportUhrenUnfinished(int zweiZonenSportUhrenUnfinished) {
		this.zweiZonenSportUhrenUnfinished = zweiZonenSportUhrenUnfinished;
	}
	
	
}
