package at.tuwien.dspace1.parts;

public abstract class SportUhr extends Uhr {
	private static final long serialVersionUID = 1L;
	
	private MetallArmband armband;
	
	public SportUhr(MetallArmband a, Gehause g, Uhrwerk u, Zeiger z1, Zeiger z2) {
		super(a, g, u, z1, z2);
		this.armband = a;
	}

	public MetallArmband getArmband() {
		return armband;
	}

	public void setArmband(MetallArmband armband) {
		this.armband = armband;
	}

}
