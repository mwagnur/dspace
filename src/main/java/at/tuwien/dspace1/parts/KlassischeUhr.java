package at.tuwien.dspace1.parts;

public class KlassischeUhr extends Uhr {
	private static final long serialVersionUID = 1L;
	
	private LederArmband armband;
	
	public KlassischeUhr(LederArmband a, Gehause g, Uhrwerk u, Zeiger z1, Zeiger z2) {
		super(a, g, u, z1, z2);
		this.armband = a;
	}

	public LederArmband getArmband() {
		return armband;
	}

	public void setArmband(LederArmband armband) {
		this.armband = armband;
	}

	@Override
	public String toString() {
		return "KlassischeUhr [" + super.dataStr() + "]" +
				"\n -> Teile [" + super.partStr() + "]";
	}
}
