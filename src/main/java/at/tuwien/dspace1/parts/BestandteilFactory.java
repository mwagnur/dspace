package at.tuwien.dspace1.parts;

import java.util.UUID;

import at.tuwien.dspace1.parts.Bestandteil;

public class BestandteilFactory {

	public static Bestandteil createPart(UUID lieferantId,
			Bestandteil.BestandteilTyp type) {
		Bestandteil part = null;

		switch (type) {
		case LEDERARMBAND:
			part = new LederArmband(lieferantId, type);
			break;
		case METALLARMBAND:
			part = new MetallArmband(lieferantId, type);
			break;
		case GEHAUSE:
			part = new Gehause(lieferantId, type);
			break;
		case UHRWERK:
			part = new Uhrwerk(lieferantId, type);
			break;
		case ZEIGER:
			part = new Zeiger(lieferantId, type);
			break;
		default:
			break;
		}

		return part;
	}

}
