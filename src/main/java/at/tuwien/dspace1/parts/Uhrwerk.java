package at.tuwien.dspace1.parts;

import java.util.UUID;

public class Uhrwerk extends Bestandteil {

	private static final long serialVersionUID = 1L;

	public Uhrwerk(UUID lieferantId, BestandteilTyp typ) {
		super(lieferantId, typ);
	}

	@Override
	public String toString() {
		return "Uhrwerk [id=" + super.id + ", lieferantId=" + super.lieferantId
				+ "]";
	}
}
