package at.tuwien.dspace1.parts;

public class NormaleSportUhr extends SportUhr {
	private static final long serialVersionUID = 1L;

	public NormaleSportUhr(MetallArmband a, Gehause g, Uhrwerk u, Zeiger z1,
			Zeiger z2) {
		super(a, g, u, z1, z2);
	}

	@Override
	public String toString() {
		return "NormaleSportUhr [" + super.dataStr() + "]" +
				"\n -> Teile [" + super.partStr() + "]";
	}
}
