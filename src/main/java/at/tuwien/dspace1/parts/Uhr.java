package at.tuwien.dspace1.parts;

import java.io.Serializable;

public abstract class Uhr implements Serializable {
	private long serialNo = -1;
	private long contractNo = -1;
	private int quality = -1;
	
	private long montageId = -1;
	private long qualityId = -1;
	private long logistikId = -1;
	
	private static final long serialVersionUID = 1L;

	// clock parts
	private Armband armband;
	private Gehause gehause;
	private Uhrwerk uhrwerk;
	private Zeiger zeig1;
	private Zeiger zeig2;

	public Uhr(Armband a, Gehause g, Uhrwerk u, Zeiger z1, Zeiger z2) {
		this.armband = a;
		this.gehause = g;
		this.uhrwerk = u;
		this.zeig1 = z1;
		this.zeig2 = z2;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public long getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(long serialNo) {
		this.serialNo = serialNo;
	}
	
	public long getMontageId() {
		return montageId;
	}

	public void setMontageId(long montageId) {
		this.montageId = montageId;
	}

	public long getQualityId() {
		return qualityId;
	}

	public void setQualityId(long qualityId) {
		this.qualityId = qualityId;
	}

	public long getLogistikId() {
		return logistikId;
	}

	public void setLogistikId(long logistikId) {
		this.logistikId = logistikId;
	}

	public Armband getArmband() {
		return armband;
	}

	public Gehause getGehause() {
		return gehause;
	}

	public Uhrwerk getUhrwerk() {
		return uhrwerk;
	}

	public Zeiger getZeig1() {
		return zeig1;
	}

	public Zeiger getZeig2() {
		return zeig2;
	}

	@Override
	public String toString() {
		return "Uhr [" + this.dataStr() + "]" +
				"\n -> Teile [" + this.partStr() + "]";
	}
	
	public String dataStr() {
			return "serialNo=" + this.serialNo +
				", contractNo=" + this.contractNo +
				", quality=" + this.quality +
				", Monteur=" + this.montageId + 
				", QualityControl=" + this.qualityId +
				", Logistiker=" + this.logistikId;
	}
	
	public String partStr() {
		return this.armband.toString() + " " + 
				this.gehause.toString() + " " + 
				this.uhrwerk.toString() + " " + 
				this.zeig1.toString() + " " + 
				this.zeig2.toString();
	}

	public long getContractNo() {
		return contractNo;
	}

	public void setContractNo(long contractNo) {
		this.contractNo = contractNo;
	}

}
