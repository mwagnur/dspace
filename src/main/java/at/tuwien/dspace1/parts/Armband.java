package at.tuwien.dspace1.parts;

import java.util.UUID;

public class Armband extends Bestandteil {

	private static final long serialVersionUID = 1L;

	public Armband(UUID lieferantId, BestandteilTyp typ) {
		super(lieferantId, typ);
	}

	@Override
	public String toString() {
		return "Armband [id=" + super.id + ", lieferantId=" + super.lieferantId
				+ "]";
	}
}
