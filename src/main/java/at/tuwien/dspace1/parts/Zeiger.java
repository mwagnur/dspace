package at.tuwien.dspace1.parts;

import java.util.UUID;

public class Zeiger extends Bestandteil {

	private static final long serialVersionUID = 1L;

	public Zeiger(UUID lieferantId, BestandteilTyp typ) {
		super(lieferantId, typ);
	}

	@Override
	public String toString() {
		return "Zeiger [id=" + super.id + ", lieferantId=" + super.lieferantId
				+ "]";
	}
}
