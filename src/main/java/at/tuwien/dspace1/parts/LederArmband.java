package at.tuwien.dspace1.parts;

import java.util.UUID;

public class LederArmband extends Armband {
	private static final long serialVersionUID = 1L;

	public LederArmband(UUID lieferantId, BestandteilTyp typ) {
		super(lieferantId, typ);
	}

	@Override
	public String toString() {
		return "LederArmband [id=" + super.id + ", lieferantId=" + super.lieferantId
				+ "]";
	}
}
