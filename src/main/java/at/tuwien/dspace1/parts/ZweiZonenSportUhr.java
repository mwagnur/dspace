package at.tuwien.dspace1.parts;

public class ZweiZonenSportUhr extends SportUhr {
	private static final long serialVersionUID = 1L;
	
	private Zeiger zeiger3;
	
	public ZweiZonenSportUhr(MetallArmband a, Gehause g, Uhrwerk u, Zeiger z1,
			Zeiger z2, Zeiger z3) {
		super(a, g, u, z1, z2);
		this.zeiger3 = z3;
	}

	public Zeiger getZeiger3() {
		return zeiger3;
	}

	public void setZeiger3(Zeiger zeiger3) {
		this.zeiger3 = zeiger3;
	}

	@Override
	public String toString() {
		return "ZweiZonenSportUhr [" + super.dataStr() + "]" +
				"\n -> Teile [" + super.partStr() + " " + this.zeiger3.toString() + "]";
	}
}
