package at.tuwien.dspace1.parts;

import java.io.Serializable;
import java.util.UUID;

//import at.tuwien.dspace1.space.Lieferant;

//TODO maybe subclasses instead
public class Bestandteil implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum BestandteilTyp {
		GEHAUSE, UHRWERK, ZEIGER, LEDERARMBAND, METALLARMBAND
	}

	protected final UUID id;
	protected final UUID lieferantId;
	protected BestandteilTyp typ;

	public Bestandteil(UUID lieferantId, BestandteilTyp typ) {
		this.id = UUID.randomUUID();
		this.lieferantId = lieferantId;
		this.typ = typ;
	}

	public BestandteilTyp getTyp() {
		return typ;
	}

	public void setTyp(BestandteilTyp typ) {
		this.typ = typ;
	}

	public UUID getId() {
		return id;
	}

	public UUID getLieferantId() {
		return lieferantId;
	}

	@Override
	public String toString() {
		return "Bestandteil [id=" + id + ", lieferantId=" + lieferantId
				+ ", typ=" + typ + "]";
	}

}
