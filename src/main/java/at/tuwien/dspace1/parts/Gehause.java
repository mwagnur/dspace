package at.tuwien.dspace1.parts;

import java.util.UUID;

public class Gehause extends Bestandteil {

	private static final long serialVersionUID = 1L;

	public Gehause(UUID lieferantId, BestandteilTyp typ) {
		super(lieferantId, typ);
	}

	@Override
	public String toString() {
		return "Gehause [id=" + super.id + ", lieferantId=" + super.lieferantId
				+ "]";
	}
}
