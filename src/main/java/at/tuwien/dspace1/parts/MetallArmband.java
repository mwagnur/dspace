package at.tuwien.dspace1.parts;

import java.util.UUID;

public class MetallArmband extends Armband {
	private static final long serialVersionUID = 1L;

	public MetallArmband(UUID lieferantId, BestandteilTyp typ) {
		super(lieferantId, typ);
	}

	@Override
	public String toString() {
		return "MetallArmband [id=" + super.id + ", lieferantId=" + super.lieferantId
				+ "]";
	}
}
