package at.tuwien.dspace1.jms.dto;

import java.util.List;
import java.util.UUID;

import at.tuwien.dspace1.parts.Bestandteil;

public class AssemblyDTO extends GenericDTO {
	private static final long serialVersionUID = 1L;

	private long serialNo;
	private List<Bestandteil> bestandteile;
	private int auftragId;

	public AssemblyDTO(UUID assemblyId, List<Bestandteil> bestandteile) {
		super(assemblyId);
		this.bestandteile = bestandteile;
	}

	public List<Bestandteil> getBestandteile() {
		return bestandteile;
	}

	public void setBestandteile(List<Bestandteil> bestandteile) {
		this.bestandteile = bestandteile;
	}

	public long getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(long serialNo) {
		this.serialNo = serialNo;
	}

	public int getAuftragId() {
		return auftragId;
	}

	public void setAuftragId(int auftragId) {
		this.auftragId = auftragId;
	}
	
	

}
