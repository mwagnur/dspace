package at.tuwien.dspace1.jms.dto;

import java.util.UUID;

import at.tuwien.dspace1.parts.Uhr;

public class LogistikDTO extends GenericDTO {
	private static final long serialVersionUID = 1L;

	private Uhr finalUhr;
	private boolean trash;

	public LogistikDTO(UUID id, Uhr uhr) {
		super(id);
		this.finalUhr = uhr;

	}

	public Uhr getFinalUhr() {
		return finalUhr;
	}

	public void setFinalUhr(Uhr finalUhr) {
		this.finalUhr = finalUhr;
	}
	
	public boolean isTrash() {
		return trash;
	}

	public void setTrash(boolean trash) {
		this.trash = trash;
	}

	@Override
	public String toString() {
		return "LogistikDTO [finalUhr=" + finalUhr + ", trash=" + trash + "]";
	}


}
