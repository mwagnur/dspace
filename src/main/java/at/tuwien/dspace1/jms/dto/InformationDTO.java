package at.tuwien.dspace1.jms.dto;

import java.io.Serializable;
import java.util.List;

import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.shared.Auftrag;

public class InformationDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private long lederArmbandCount;
	private long metallArmbandCount;
	private long zeigerCount;
	private long uhrwerkCount;
	private long gehauseCount;

	private long qualityClockCount;
	private long logistikClockCount;
	private long finalClockCount;
	private long trashClockCount;

	private List<Uhr> qualityClockList;
	private List<Uhr> logistikClockList;
	private List<Uhr> finalClockList;
	private List<Uhr> trashClockList;
	
	private List<Auftrag> activeAuftragList;
	private List<Auftrag> completedAuftragList;

	public long getLederArmbandCount() {
		return lederArmbandCount;
	}

	public void setLederArmbandCount(long c) {
		this.lederArmbandCount = c;
	}
	
	public long getMetallArmbandCount() {
		return metallArmbandCount;
	}

	public void setMetallArmbandCount(long c) {
		this.metallArmbandCount = c;
	}

	public long getZeigerCount() {
		return zeigerCount;
	}

	public void setZeigerCount(long zeigerCount) {
		this.zeigerCount = zeigerCount;
	}

	public long getUhrwerkCount() {
		return uhrwerkCount;
	}

	public void setUhrwerkCount(long uhrwerkCount) {
		this.uhrwerkCount = uhrwerkCount;
	}

	public long getGehauseCount() {
		return gehauseCount;
	}

	public void setGehauseCount(long gehauseCount) {
		this.gehauseCount = gehauseCount;
	}

	public long getQualityClockCount() {
		return qualityClockCount;
	}

	public void setQualityClockCount(long qualityClockCount) {
		this.qualityClockCount = qualityClockCount;
	}

	public long getLogistikClockCount() {
		return logistikClockCount;
	}

	public void setLogistikClockCount(long logistikClockCount) {
		this.logistikClockCount = logistikClockCount;
	}

	public long getFinalClockCount() {
		return finalClockCount;
	}

	public void setFinalClockCount(long finalClockCount) {
		this.finalClockCount = finalClockCount;
	}

	public List<Uhr> getQualityClockList() {
		return qualityClockList;
	}

	public void setQualityClockList(List<Uhr> qualityClockList) {
		this.qualityClockList = qualityClockList;
	}

	public List<Uhr> getLogistikClockList() {
		return logistikClockList;
	}

	public void setLogistikClockList(List<Uhr> logistikClockList) {
		this.logistikClockList = logistikClockList;
	}

	public List<Uhr> getFinalClockList() {
		return finalClockList;
	}

	public void setFinalClockList(List<Uhr> finalClockList) {
		this.finalClockList = finalClockList;
	}

	public long getTrashClockCount() {
		return trashClockCount;
	}

	public void setTrashClockCount(long trashClockCount) {
		this.trashClockCount = trashClockCount;
	}

	public List<Uhr> getTrashClockList() {
		return trashClockList;
	}

	public void setTrashClockList(List<Uhr> trashClockList) {
		this.trashClockList = trashClockList;
	}
	
	public List<Auftrag> getActiveAuftragList() {
		return activeAuftragList;
	}

	public void setActiveAuftragList(List<Auftrag> activeAuftragList) {
		this.activeAuftragList = activeAuftragList;
	}

	public List<Auftrag> getCompletedAuftragList() {
		return completedAuftragList;
	}

	public void setCompletedAuftragList(List<Auftrag> completedAuftragList) {
		this.completedAuftragList = completedAuftragList;
	}

	@Override
	public String toString() {
		return "InformationDTO [lederArmbandCount=" + lederArmbandCount 
				+ ", metallArmbandCount=" + metallArmbandCount
				+ ", zeigerCount=" + zeigerCount + ", uhrwerkCount="
				+ uhrwerkCount + ", gehauseCount=" + gehauseCount
				+ ", qualityClockCount=" + qualityClockCount
				+ ", logistikClockCount=" + logistikClockCount
				+ ", finalClockCount=" + finalClockCount + ", trashClockCount="
				+ trashClockCount + ", qualityClockList=" + qualityClockList
				+ ", logistikClockList=" + logistikClockList
				+ ", finalClockList=" + finalClockList + ", trashClockList="
				+ trashClockList + "]";
	}

}
