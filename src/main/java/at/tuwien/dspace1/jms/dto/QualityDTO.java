package at.tuwien.dspace1.jms.dto;

import java.util.UUID;

import at.tuwien.dspace1.parts.Uhr;

public class QualityDTO extends GenericDTO {
	private static final long serialVersionUID = 1L;

	private Uhr uhr;

	public QualityDTO(UUID qualityId, Uhr uhr) {
		super(qualityId);
		this.uhr = uhr;
	}

	public Uhr getUhr() {
		return uhr;
	}

	public void setUhr(Uhr uhr) {
		this.uhr = uhr;
	}

}
