package at.tuwien.dspace1.jms.dto;

import java.io.Serializable;

import at.tuwien.dspace1.parts.Uhr;

public class KundenbetreuerDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Uhr uhr;
	private GrossHandlerDTO grossHandlerDTO;

	public KundenbetreuerDTO(Uhr uhr, GrossHandlerDTO grossHandlerDTO) {
		super();
		this.uhr = uhr;
		this.grossHandlerDTO = grossHandlerDTO;
	}

	public Uhr getUhr() {
		return uhr;
	}

	public void setUhr(Uhr uhr) {
		this.uhr = uhr;
	}

	public GrossHandlerDTO getGrossHandlerDTO() {
		return grossHandlerDTO;
	}

	public void setGrossHandlerDTO(GrossHandlerDTO grossHandlerDTO) {
		this.grossHandlerDTO = grossHandlerDTO;
	}

}
