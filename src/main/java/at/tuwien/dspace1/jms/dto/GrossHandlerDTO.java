package at.tuwien.dspace1.jms.dto;

import java.io.Serializable;

import javax.jms.Destination;

public class GrossHandlerDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int klassischeUhrenMax;
	private int normaleSportUhrenMax;
	private int zweiZonenSportUhrenMax;

	private Destination handlerDestination;

	private String handlerURI;
	
	private int klassischeUhrenCurrent = 0;
	private int normaleSportUhrenCurrent = 0;
	private int zweiZonenSportUhrenCurrent = 0;

	public GrossHandlerDTO(int klassischeUhrenMax, int normaleSportUhrenMax,
			int zweiZonenSportUhrenMax) {
		super();
		this.klassischeUhrenMax = klassischeUhrenMax;
		this.normaleSportUhrenMax = normaleSportUhrenMax;
		this.zweiZonenSportUhrenMax = zweiZonenSportUhrenMax;
	}

	public int getKlassischeUhrenMax() {

		return klassischeUhrenMax;
	}

	public void setKlassischeUhrenMax(int klassischeUhrenMax) {
		this.klassischeUhrenMax = klassischeUhrenMax;
	}

	public int getNormaleSportUhrenMax() {
		return normaleSportUhrenMax;
	}

	public void setNormaleSportUhrenMax(int normaleSportUhrenMax) {
		this.normaleSportUhrenMax = normaleSportUhrenMax;
	}

	public int getZweiZonenSportUhrenMax() {
		return zweiZonenSportUhrenMax;
	}

	public void setZweiZonenSportUhrenMax(int zweiZonenSportUhrenMax) {
		this.zweiZonenSportUhrenMax = zweiZonenSportUhrenMax;
	}

	public Destination getHandlerDestination() {
		return handlerDestination;
	}

	public void setHandlerDestination(Destination handlerDestination) {
		this.handlerDestination = handlerDestination;
	}

	public String getHandlerURI() {
		return handlerURI;
	}

	public void setHandlerURI(String handlerURI) {
		this.handlerURI = handlerURI;
	}
	
	public int getKlassischeUhrenCurrent() {
		return klassischeUhrenCurrent;
	}

	public void setKlassischeUhrenCurrent(int klassischeUhrenCurrent) {
		this.klassischeUhrenCurrent = klassischeUhrenCurrent;
	}

	public int getNormaleSportUhrenCurrent() {
		return normaleSportUhrenCurrent;
	}

	public void setNormaleSportUhrenCurrent(int normaleSportUhrenCurrent) {
		this.normaleSportUhrenCurrent = normaleSportUhrenCurrent;
	}

	public int getZweiZonenSportUhrenCurrent() {
		return zweiZonenSportUhrenCurrent;
	}

	public void setZweiZonenSportUhrenCurrent(int zweiZonenSportUhrenCurrent) {
		this.zweiZonenSportUhrenCurrent = zweiZonenSportUhrenCurrent;
	}

	@Override
	public String toString() {
		return "GrossHandlerDTO [klassischeUhrenMax=" + klassischeUhrenMax
				+ ", normaleSportUhrenMax=" + normaleSportUhrenMax
				+ ", zweiZonenSportUhrenMax=" + zweiZonenSportUhrenMax
				+ ", handlerDestination=" + handlerDestination
				+ ", handlerURI=" + handlerURI + "]";
	}

}
