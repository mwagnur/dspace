package at.tuwien.dspace1.jms.dto;

import java.io.Serializable;
import java.util.UUID;

public abstract class GenericDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private UUID id;

	public GenericDTO(UUID id) {
		super();
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

}
