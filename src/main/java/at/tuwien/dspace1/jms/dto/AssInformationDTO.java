package at.tuwien.dspace1.jms.dto;

import java.io.Serializable;

public class AssInformationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long lederArmbandCount;
	private long metallArmbandCount;
	private long zeigerCount;
	private long uhrwerkCount;
	private long gehauseCount;

	public AssInformationDTO() {

	}

	public AssInformationDTO(long lederArmbandCount, long metallArmbandCount,
			long zeigerCount, long uhrwerkCount, long gehauseCount) {
		super();
		this.lederArmbandCount = lederArmbandCount;
		this.metallArmbandCount = metallArmbandCount;
		this.zeigerCount = zeigerCount;
		this.uhrwerkCount = uhrwerkCount;
		this.gehauseCount = gehauseCount;
	}

	public long getLederArmbandCount() {
		return lederArmbandCount;
	}

	public void setLederArmbandCount(long lederArmbandCount) {
		this.lederArmbandCount = lederArmbandCount;
	}

	public long getMetallArmbandCount() {
		return metallArmbandCount;
	}

	public void setMetallArmbandCount(long metallArmbandCount) {
		this.metallArmbandCount = metallArmbandCount;
	}

	public long getZeigerCount() {
		return zeigerCount;
	}

	public void setZeigerCount(long zeigerCount) {
		this.zeigerCount = zeigerCount;
	}

	public long getUhrwerkCount() {
		return uhrwerkCount;
	}

	public void setUhrwerkCount(long uhrwerkCount) {
		this.uhrwerkCount = uhrwerkCount;
	}

	public long getGehauseCount() {
		return gehauseCount;
	}

	public void setGehauseCount(long gehauseCount) {
		this.gehauseCount = gehauseCount;
	}

}
