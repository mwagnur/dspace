package at.tuwien.dspace1.jms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Stack;
import java.util.UUID;

import at.tuwien.dspace1.jms.dto.AssInformationDTO;
import at.tuwien.dspace1.jms.dto.AssemblyDTO;
import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.parts.KlassischeUhr;
import at.tuwien.dspace1.parts.NormaleSportUhr;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.ZweiZonenSportUhr;
import at.tuwien.dspace1.shared.Auftrag;
import at.tuwien.dspace1.shared.Auftrag.Priority;

/**
 * representing underlying data for the Server to use
 * 
 * @author Wagi
 * 
 */
public class ServerDataManager {

	// data structures
	private List<Stack<Bestandteil>> assembly;
	private List<Uhr> trash;
	private Stack<Uhr> quality;
	private List<Uhr> logistik;
	private List<Uhr> delivered;

	private long serialNo = 1;

	private List<LinkedList<Auftrag>> auftraege;
	private List<LinkedList<Auftrag>> auftraegeInWork;

	private List<Auftrag> completedAuftraege;

	private boolean lastJobWasContract = false;

	private Random rand = new Random();

	public enum UhrTyp {
		KLASSISCH, NORMAL_SPORTLICH, ZWEIZONEN_SPORTLICH
	};

	public ServerDataManager() {
		assembly = new ArrayList<Stack<Bestandteil>>();
		trash = new ArrayList<Uhr>();
		for (int i = 0; i < BestandteilTyp.values().length; i++) {
			assembly.add(new Stack<Bestandteil>());
		}
		quality = new Stack<Uhr>();
		logistik = new ArrayList<Uhr>();
		delivered = new ArrayList<Uhr>();

		auftraege = new ArrayList<LinkedList<Auftrag>>();
		for (int i = 0; i < Auftrag.Priority.values().length; i++) {
			auftraege.add(new LinkedList<Auftrag>());
		}

		auftraegeInWork = new ArrayList<LinkedList<Auftrag>>();
		for (int i = 0; i < Auftrag.Priority.values().length; i++) {
			auftraegeInWork.add(new LinkedList<Auftrag>());
		}
		completedAuftraege = new ArrayList<Auftrag>();
	}

	public Uhr getUhrForGrossHaendler(UhrTyp typ) {
		for (Uhr curr : delivered) {

			if (curr.getContractNo() == -1 || curr.getContractNo() == 0) {

				if (typ.equals(UhrTyp.KLASSISCH)) {

					if (curr instanceof KlassischeUhr) {
						System.out.println("found uhr for grosshaendler: "
								+ curr);
						return curr;
					}

				} else if (typ.equals(UhrTyp.NORMAL_SPORTLICH)) {
					if (curr instanceof NormaleSportUhr) {
						System.out.println("found uhr for grosshaendler: "
								+ curr);
						return curr;
					}
				} else if (typ.equals(UhrTyp.ZWEIZONEN_SPORTLICH)) {
					if (curr instanceof ZweiZonenSportUhr) {
						System.out.println("found uhr for grosshaendler: "
								+ curr);
						return curr;
					}
				} else {
					System.err.println("wrong uhr");
				}
			}

		}

		return null;

	}

	public List<Auftrag> getActiveAuftraege() {

		List<Auftrag> ret = new ArrayList<Auftrag>();

		for (LinkedList<Auftrag> currList : auftraegeInWork) {
			ret.addAll(currList);
		}

		for (LinkedList<Auftrag> currList : auftraege) {
			ret.addAll(currList);
		}

		return ret;
	}

	public List<Auftrag> getCompletedAuftraege() {
		return completedAuftraege;
	}

	private Bestandteil takePartFromAssembly(BestandteilTyp typ) {
		return assembly.get(typ.ordinal()).pop();
	}

	private Auftrag getFirstAuftrag(Auftrag.Priority priority) {
		return auftraege.get(priority.ordinal()).peekFirst();
	}

	private Auftrag getAuftrag(int index, Auftrag.Priority priority) {
		return auftraege.get(priority.ordinal()).get(index);
	}

	private Auftrag takeFirstAuftrag(Auftrag.Priority priority) {
		return auftraege.get(priority.ordinal()).pollFirst();
	}

	private void addAuftragInWork(Auftrag auftrag, Auftrag.Priority priority) {
		auftraegeInWork.get(priority.ordinal()).add(auftrag);
	}

	private List<Bestandteil> takePartsForUhrTyp(UhrTyp typ) {
		List<Bestandteil> list = new ArrayList<Bestandteil>();

		list.add(takePartFromAssembly(BestandteilTyp.GEHAUSE));
		list.add(takePartFromAssembly(BestandteilTyp.UHRWERK));
		list.add(takePartFromAssembly(BestandteilTyp.ZEIGER));
		list.add(takePartFromAssembly(BestandteilTyp.ZEIGER));

		switch (typ) {
		case KLASSISCH:
			list.add(takePartFromAssembly(BestandteilTyp.LEDERARMBAND));
			break;
		case NORMAL_SPORTLICH:
			list.add(takePartFromAssembly(BestandteilTyp.METALLARMBAND));
			break;
		case ZWEIZONEN_SPORTLICH:
			list.add(takePartFromAssembly(BestandteilTyp.METALLARMBAND));
			list.add(takePartFromAssembly(BestandteilTyp.ZEIGER));
			break;
		default:
			return null;
		}

		return list;
	}

	/**
	 * takes parts from assembly with contracts enabled
	 * 
	 * @return
	 */
	public AssemblyDTO takePartsFromAssemblyWithContracts() {
		ArrayList<Integer> possibilities = new ArrayList<Integer>();

		long gehause = getBestandteilCount(BestandteilTyp.GEHAUSE);
		long uhrwerk = getBestandteilCount(BestandteilTyp.UHRWERK);
		long zeiger = getBestandteilCount(BestandteilTyp.ZEIGER);
		long lederArmband = getBestandteilCount(BestandteilTyp.LEDERARMBAND);
		long metallArmband = getBestandteilCount(BestandteilTyp.METALLARMBAND);

		if ((gehause > 0) && (uhrwerk > 0) && (zeiger > 1)) {
			if (lederArmband > 0) {
				// KlassischeUhr = 0q
				possibilities.add(0);
			}
			if (metallArmband > 0) {
				// NormaleSportUhr = 1
				possibilities.add(1);

				if (zeiger > 2) {
					// ZweiZonenSportUhr = 2
					possibilities.add(2);
				}
			}
		}

		System.out.println("possibilities determined: " + possibilities);

		if (possibilities.size() < 1) {
			/* NOTHING TO DO HERE */
			/* WE HAVE NO CHOICE */

			System.out.println("building a clock currently not possible.");

			return null;
		}

		// check if HIGH or NORMAL contracts are to be worked
		Auftrag auftrag = determineImportantAuftragToWorkOn(possibilities);

		List<Bestandteil> list = new ArrayList<Bestandteil>();

		if (auftrag != null) {
			list = takePartsAccordingToAuftrag(possibilities, auftrag);
		} else {

			// no HIGH / NORMAL contracts, proceed with normal assembly/LOW
			list = takePartsNormalProductionAndLowAuftrag(possibilities);
		}

		if (list == null) {
			System.out.println("no list could be created - no clock possible");
			return null;
		}

		AssemblyDTO ass = new AssemblyDTO(UUID.randomUUID(), list);
		ass.setSerialNo(getAndIncrSerialNo());
		if (auftrag != null)
			ass.setAuftragId(auftrag.getId());

		return ass;

	}

	private boolean checkAuftragPossible(Auftrag auftrag,
			ArrayList<Integer> possibilities) {
		int klass = auftrag.getKlassischeUhren();
		int normSport = auftrag.getNormaleSportUhren();
		int zweiSport = auftrag.getZweiZonenSportUhren();

		if (klass > 0 && possibilities.contains(0)) {
			return true;
		} else if (normSport > 0 && possibilities.contains(1)) {
			return true;
		} else if (zweiSport > 0 && possibilities.contains(2)) {
			return true;
		} else {
			return false;
		}
	}

	private List<Bestandteil> takePartsAccordingToAuftrag(
			ArrayList<Integer> possibilities, Auftrag auftrag) {
		List<Bestandteil> list;
		int klass = auftrag.getKlassischeUhren();
		int normSport = auftrag.getNormaleSportUhren();
		int zweiSport = auftrag.getZweiZonenSportUhren();

		if (klass > 0 && possibilities.contains(0)) {
			list = takePartsForUhrTyp(UhrTyp.KLASSISCH);

			auftrag.setKlassischeUhren(klass - 1);

		} else if (normSport > 0 && possibilities.contains(1)) {
			list = takePartsForUhrTyp(UhrTyp.NORMAL_SPORTLICH);

			auftrag.setNormaleSportUhren(normSport - 1);

		} else if (zweiSport > 0 && possibilities.contains(2)) {
			list = takePartsForUhrTyp(UhrTyp.ZWEIZONEN_SPORTLICH);

			auftrag.setZweiZonenSportUhren(zweiSport - 1);

		} else {
			// no clock possible for auftrag; go for normal production
			System.out.println("no clock possible for auftrag: "
					+ auftrag.getId());
			list = takePartsNormalProductionAndLowAuftrag(possibilities);
		}
		if (auftrag.getKlassischeUhren() == 0
				&& auftrag.getNormaleSportUhren() == 0
				&& auftrag.getZweiZonenSportUhren() == 0) {
			auftrag.markCompleted();
			completedAuftraege.add(auftrag);

			if (auftraegeInWork.get(auftrag.getPriority().ordinal()).contains(
					auftrag))
				auftraegeInWork.get(auftrag.getPriority().ordinal()).remove(
						auftrag);

			System.out.println("auftrag marked complete");
		}
		return list;
	}

	private List<Bestandteil> takePartsNormalProductionAndLowAuftrag(

	ArrayList<Integer> possibilities) {
		List<Bestandteil> list;

		if (!lastJobWasContract) {
			lastJobWasContract = true;

			Auftrag auftrag = determineNextAuftragForPriority(possibilities,
					Priority.LOW);

			if (auftrag == null) {
				// there is no LOW Auftrag, continue with normal production

				list = takePartsNormalProductionAndLowAuftrag(possibilities);
			} else {
				// there IS LOW Auftrag, process
				list = takePartsAccordingToAuftrag(possibilities, auftrag);
			}

		} else {

			switch (possibilities.get(rand.nextInt(possibilities.size()))) {
			case 0:
				list = takePartsForUhrTyp(UhrTyp.KLASSISCH);
				break;
			case 1:
				list = takePartsForUhrTyp(UhrTyp.NORMAL_SPORTLICH);
				break;
			case 2:
				list = takePartsForUhrTyp(UhrTyp.ZWEIZONEN_SPORTLICH);
				break;
			default:
				return null;
			}

			lastJobWasContract = false;

		}
		return list;
	}

	/**
	 * excluding LOW auftraege
	 * 
	 * @return null if no InWork / HIGH / NORMAL Auftraege
	 */
	private Auftrag determineImportantAuftragToWorkOn(
			ArrayList<Integer> possibilities) {

		// Check if HIGH auftrag possible
		Auftrag auftrag = determineNextAuftragForPriority(possibilities,
				Priority.HIGH);

		if (auftrag != null) {
			return auftrag;
		}
		// Check if NORMAL auftrag possible
		auftrag = determineNextAuftragForPriority(possibilities,
				Priority.NORMAL);

		return auftrag;
	}

	private Auftrag determineNextAuftragForPriority(
			ArrayList<Integer> possibilities, Priority priority) {

		for (Auftrag currAuftragInWork : auftraegeInWork
				.get(priority.ordinal())) {

			if (checkAuftragPossible(currAuftragInWork, possibilities)) {
				System.out.println("determined inWork " + priority.toString()
						+ " queue auftrag: " + currAuftragInWork);
				return currAuftragInWork;

			} else {
				System.out.println("auftrag (in work " + priority.toString()
						+ ") not possible, id: " + currAuftragInWork.getId());
			}

		}

		// no auftrage inWork , take a new one out of priority queue
		Auftrag auftrag = takeNextAuftragInAuftraege(possibilities, priority);
		return auftrag;
	}

	private Auftrag takeNextAuftragInAuftraege(
			ArrayList<Integer> possibilities, Priority priority) {
		int sizeHighAuftraege = auftraege.get(priority.ordinal()).size();
		Auftrag auftrag = null;

		for (int i = 0; i < sizeHighAuftraege; i++) {

			auftrag = getAuftrag(i, priority);

			if (checkAuftragPossible(auftrag, possibilities)) {
				auftrag = takeFirstAuftrag(priority);
				addAuftragInWork(auftrag, priority);
				System.out.println("determined " + priority.toString()
						+ " queue auftrag: " + auftrag);
				return auftrag;

			} else {
				System.out.println("auftrag not possible for id: "
						+ auftrag.getId() + " - get next auftrag in "
						+ priority.toString() + " queue to check");
			}
		}
		return auftrag;
	}

	public long getAndIncrSerialNo() {
		return serialNo++;
	}

	public synchronized List<Bestandteil> takePartsFromAssembly(
			int gehauseCount, int uhrwerkCount, int zeigerCount,
			int lederArmbandCount, int metallArmbandCount) {
		// GEHAUSE, UHRWERK, ZEIGER, LEDERARMBAND, METALLARMBAND

		if (assembly.get(BestandteilTyp.GEHAUSE.ordinal()).size() < gehauseCount
				|| assembly.get(BestandteilTyp.UHRWERK.ordinal()).size() < uhrwerkCount
				|| assembly.get(BestandteilTyp.ZEIGER.ordinal()).size() < zeigerCount
				|| assembly.get(BestandteilTyp.LEDERARMBAND.ordinal()).size() < lederArmbandCount
				|| assembly.get(BestandteilTyp.METALLARMBAND.ordinal()).size() < metallArmbandCount) {
			System.out.println("requested parts not in assembly; abort");
			return null;
		}

		List<Bestandteil> list = new ArrayList<Bestandteil>();

		for (int i = 0; i < assembly.size(); i++) {

			Stack<Bestandteil> currPartStack = assembly.get(i);

			switch (i) {
			case 0:

				for (int k = 0; k < gehauseCount; k++) {
					Bestandteil currPart = currPartStack.pop();
					list.add(currPart);
				}
				break;
			case 1:

				for (int k = 0; k < uhrwerkCount; k++) {
					Bestandteil currPart = currPartStack.pop();
					list.add(currPart);
				}
				break;
			case 2:

				for (int k = 0; k < zeigerCount; k++) {
					Bestandteil currPart = currPartStack.pop();
					list.add(currPart);
				}
				break;
			case 3:

				for (int k = 0; k < lederArmbandCount; k++) {
					Bestandteil currPart = currPartStack.pop();
					list.add(currPart);
				}
				break;
			case 4:

				for (int k = 0; k < metallArmbandCount; k++) {
					Bestandteil currPart = currPartStack.pop();
					list.add(currPart);
				}
				break;
			default:
				System.err
						.println("unexpected error in assembly data structure");
				break;
			}

		}

		return list;

	}

	public synchronized void addAuftrag(Auftrag auftrag) {
		auftraege.get(auftrag.getPriority().ordinal()).add(auftrag);
	}

	public synchronized void addClockToQuality(Uhr clock) {
		quality.add(clock);
	}

	public synchronized void addPartToAssembly(Bestandteil part) {
		assembly.get(part.getTyp().ordinal()).push(part);
	}

	public synchronized void addClockToLogistik(Uhr clock) {
		logistik.add(clock);
	}

	public synchronized void addClockToDelivered(Uhr clock) {
		delivered.add(clock);
	}

	public synchronized void addClockToTrash(Uhr clock) {
		trash.add(clock);
	}

	public List<Bestandteil> getPartsFromAssembly() {
		List<Bestandteil> parts = new ArrayList<Bestandteil>();
		for (Stack<Bestandteil> currStack : assembly) {
			for (Bestandteil curr : currStack) {
				parts.add(curr);
			}
		}
		return parts;
	}

	public List<Uhr> getClocksFromQuality() {
		return quality;
	}

	public List<Uhr> getClocksFromLogistik() {
		return logistik;
	}

	public List<Uhr> getClocksFromDelivered() {
		return delivered;
	}

	public List<Uhr> getClocksFromTrash() {
		return trash;
	}

	public int getBestandteilCount(BestandteilTyp typ) {
		return assembly.get(typ.ordinal()).size();
	}

	public synchronized Uhr takeClockFromQuality() {
		if (quality.isEmpty()) {
			return null;
		}

		Uhr uhr = quality.pop();
		return uhr;
	}

	public synchronized Uhr takeClockFromLogistik(int gueteFrom, int gueteTo) {
		Iterator<Uhr> iter = logistik.iterator();

		while (iter.hasNext()) {
			Uhr curr = iter.next();

			if (curr.getQuality() >= gueteFrom && curr.getQuality() <= gueteTo) {

				iter.remove();
				return curr;

			}
		}

		return null;
	}

	private boolean checkClockAssemblyPossible() {
		for (int i = 0; i < assembly.size(); i++) {

			if (i == BestandteilTyp.ZEIGER.ordinal()) {
				if (assembly.get(i).size() < 2) {
					return false;
				}
			} else {
				if (assembly.get(i).size() < 1) {
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized AssemblyDTO takePartsFromAssemblyWithoutContracts() {
		ArrayList<Integer> possibilities = new ArrayList<Integer>();

		long gehause = getBestandteilCount(BestandteilTyp.GEHAUSE);
		long uhrwerk = getBestandteilCount(BestandteilTyp.UHRWERK);
		long zeiger = getBestandteilCount(BestandteilTyp.ZEIGER);
		long lederArmband = getBestandteilCount(BestandteilTyp.LEDERARMBAND);
		long metallArmband = getBestandteilCount(BestandteilTyp.METALLARMBAND);

		if ((gehause > 0) && (uhrwerk > 0) && (zeiger > 1)) {
			if (lederArmband > 0) {
				// KlassischeUhr = 0q
				possibilities.add(0);
			}
			if (metallArmband > 0) {
				// NormaleSportUhr = 1
				possibilities.add(1);

				if (zeiger > 2) {
					// ZweiZonenSportUhr = 2
					possibilities.add(2);
				}
			}
		}

		if (possibilities.size() < 1) {
			/* NOTHING TO DO HERE */
			/* WE HAVE NO CHOICE */
			return null;
		}

		List<Bestandteil> list = new ArrayList<Bestandteil>();

		switch (possibilities.get(rand.nextInt(possibilities.size()))) {
		case 0:
			list.add(takePartFromAssembly(BestandteilTyp.LEDERARMBAND));
			break;
		case 1:
			list.add(takePartFromAssembly(BestandteilTyp.METALLARMBAND));
			break;
		case 2:
			list.add(takePartFromAssembly(BestandteilTyp.METALLARMBAND));
			list.add(takePartFromAssembly(BestandteilTyp.ZEIGER));
			break;
		default:
			return null;
		}

		AssemblyDTO ass = new AssemblyDTO(UUID.randomUUID(), list);
		ass.setSerialNo(getAndIncrSerialNo());

		return ass;
	}

	public void printAllContainersDebug() {

		System.out.println("CONTENT ASSEMBLY: ");

		for (Stack<Bestandteil> currStack : assembly) {

			System.out.println("---------");

			for (Bestandteil curr : currStack) {
				System.out.println(curr);
			}
		}

		System.out.println("CONTENT QUALITY: ");

		for (Uhr curr : quality) {
			System.out.println(curr);
		}

		System.out.println("CONTENT LOGISTIK: ");

		for (Uhr curr : logistik) {
			System.out.println(curr);
		}

		System.out.println("CONTENT DELIVERED(FINAL): ");

		for (Uhr curr : delivered) {
			System.out.println(curr);
		}

		System.out.println("------------------");

		System.out.println("AUFTRAEGE");
		System.out.println("-- INWORK");
		for (LinkedList<Auftrag> currList : auftraegeInWork) {

			for (Auftrag curr : currList) {
				System.out.println(curr);
			}
			System.out.println("---------");
		}

		System.out.println("-- NOTINWORK");
		for (LinkedList<Auftrag> currList : auftraege) {

			for (Auftrag curr : currList) {
				System.out.println(curr);
			}
			System.out.println("---------");
		}

	}
}
