package at.tuwien.dspace1.jms;

import java.io.Serializable;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import at.tuwien.dspace1.parts.Armband;
import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.parts.Gehause;
import at.tuwien.dspace1.parts.LederArmband;
import at.tuwien.dspace1.parts.MetallArmband;
import at.tuwien.dspace1.parts.Uhrwerk;
import at.tuwien.dspace1.parts.Zeiger;
import at.tuwien.dspace1.shared.ILieferant;

public class LieferantJMS implements ILieferant {
	private BestandteilTyp typ;
	private int anzahl;
	private final UUID lieferantId;

	private Connection connection;
	private Session session;
	private ConnectionFactory connectionFactory;

	private Queue serverQueue;
	private MessageProducer senderServer;

	public LieferantJMS(BestandteilTyp typ, int anzahl) {
		System.out.println("instantinating lieferantJMS with " + anzahl + " "
				+ typ.name());

		this.typ = typ;
		this.anzahl = anzahl;
		this.lieferantId = UUID.randomUUID();

		try {
			init();

		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	private void init() throws NamingException, JMSException {
		Context jndiContext = new InitialContext();

		connectionFactory = (ConnectionFactory) jndiContext
				.lookup(JMSUtil.CONNECTION_FACTORY);

		connection = connectionFactory.createConnection();

		// Create a Session
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		// montageQueue = session.createQueue("TEST.FOO");

		serverQueue = (Queue) jndiContext.lookup(JMSUtil.SERVER_QUEUE);
		senderServer = session.createProducer(serverQueue);

		connection.start();

		System.out.println("Lieferant initialized");
	}

	private void send(Serializable bestandteil) throws JMSException {

		ObjectMessage message = session.createObjectMessage();
		message.setObject(bestandteil);
		senderServer.send(message);

	}

	public void stop() throws JMSException {
		session.close();
		connection.close();
	}

	@Override
	public void processLieferung() {
		System.out.println("LieferantJMS processLieferung()");
		try {
			for (int i = 0; i < anzahl; i++) {

				Bestandteil bestandteil;

				if (typ.equals(BestandteilTyp.LEDERARMBAND)) {
					bestandteil = new LederArmband(lieferantId, typ);
				} else if (typ.equals(BestandteilTyp.METALLARMBAND)) {
					bestandteil = new MetallArmband(lieferantId, typ);
				} else if (typ.equals(BestandteilTyp.GEHAUSE)) {
					bestandteil = new Gehause(lieferantId, typ);
				} else if (typ.equals(BestandteilTyp.UHRWERK)) {
					bestandteil = new Uhrwerk(lieferantId, typ);
				} else if (typ.equals(BestandteilTyp.ZEIGER)) {
					bestandteil = new Zeiger(lieferantId, typ);
				} else {
					bestandteil = null;
					System.err.println("Error in Bestandteil creation");
					return;
				}

				//System.out.println("Sending to Server: " + bestandteil);

				send(bestandteil);

			}

			//System.out.println("sent everything");
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
