package at.tuwien.dspace1.jms;

public class JMSUtil {
	// keep synchronized with jndi.properties to properly look up the queues
	public final static String MONTAGE_QUEUE = "Montage";
	public final static String SERVER_QUEUE = "Server";
	public final static String QUALITY_QUEUE = "Quality";
	public final static String LOGISTIK_QUEUE = "Logistik";
	public final static String UI_TOPIC_QUEUE = "UI";
	public final static String BENCHMARK_TOPIC_QUEUE = "Benchmark";

	public final static String CONNECTION_FACTORY = "ConnectionFactory";
	
	public final static long WORKER_LOOP_SLEEP = 500;
	
	public final static String START_BENCHMARK = "START_BENCHMARK";
	public final static String STOP_BENCHMARK = "STOP_BENCHMARK";
	
	public static boolean benchmarkMode = false;
	public final static String PREPARE_BENCHMARK_MESSAGE = "prepareBenchmarkData";
	
	public final static String REGISTER_GROSSHAENDLER = "register_grosshaendler";
}
