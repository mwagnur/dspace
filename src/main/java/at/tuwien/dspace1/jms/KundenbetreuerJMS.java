package at.tuwien.dspace1.jms;

import java.io.Serializable;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import at.tuwien.dspace1.jms.dto.KundenbetreuerDTO;

public class KundenbetreuerJMS implements MessageListener {

	private static boolean started = false;

	private Connection connection;
	private static Session session;
	private ConnectionFactory connectionFactory;
	private Context jndiContext;

	private static MessageProducer senderServer;

	private Destination tempDest;
	private MessageConsumer receiverResponse;

	public KundenbetreuerJMS() {

	}

	public void init() throws JMSException, NamingException {
		jndiContext = new InitialContext();

		connectionFactory = (ConnectionFactory) jndiContext
				.lookup(JMSUtil.CONNECTION_FACTORY);

		connection = connectionFactory.createConnection();
		connection.setClientID(UUID.randomUUID().toString());

		// Create a Session
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		Queue serverQueue = (Queue) jndiContext.lookup(JMSUtil.SERVER_QUEUE);

		senderServer = session.createProducer(serverQueue);
		
		tempDest = session.createTemporaryQueue();
		receiverResponse = session.createConsumer(tempDest);
		receiverResponse.setMessageListener(this);

		Topic benchmarkTopicQueue;
		benchmarkTopicQueue = (Topic) jndiContext
				.lookup(JMSUtil.BENCHMARK_TOPIC_QUEUE);

		MessageConsumer subscriberTopic = session.createDurableSubscriber(
				benchmarkTopicQueue, "benchmark");

		subscriberTopic.setMessageListener(this);

		connection.start();

		System.out.println("finished INIT");
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
		}

		System.out.println("STARTING KUNDENBETREUER WORKER");

		KundenbetreuerJMS kundenbetreuer = new KundenbetreuerJMS();

		try {
			kundenbetreuer.init();

			System.out
					.println("STARTED KUNDENBETREUER WORKER - waiting for signal");

		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	public void sendToServer(Serializable obj) throws JMSException {

		ObjectMessage message = session.createObjectMessage();

		message.setJMSReplyTo(tempDest);
		message.setObject(obj);
		senderServer.send(message);
		System.out.println("sent to server: " + obj);
	}

	private void startMainLoopThread() throws JMSException {

		System.out.println("starting main loop thread");
		Thread foo = new Thread() {

			@Override
			public void run() {

				while (started) {
					System.out.println("checking for work..");

					try {
						KundenbetreuerJMS.this
								.sendToServer(ServerJMS.CMD_KUNDENBETREUER_REQUEST);

					} catch (JMSException e1) {
						e1.printStackTrace();
					}

					try {
						Thread.sleep(1000);

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println("Worker thread stopped");
			}
		};
		foo.start();

	}

	private static void usage() {
		System.out.println("Usage: Kundenbetreuer <ID>");
		System.exit(-1);
	}

	private void sendMessage(Destination destination, Serializable msg) {
		try {

			ConnectionFactory factory = (ConnectionFactory) jndiContext
					.lookup(JMSUtil.CONNECTION_FACTORY);

			Connection connection = factory.createConnection();
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			MessageProducer sender = session.createProducer(destination);

			ObjectMessage message = session.createObjectMessage();
			message.setObject(msg);
			sender.send(message);

			connection.close();
			System.out.println("sent Message to some destination");

		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(Message message) {
		ObjectMessage om = (ObjectMessage) message;
		try {
			Serializable object = om.getObject();
			System.out.println("Received: " + object);

			if (object instanceof String) {
				System.out.println("Received string message: " + object);
				String strMsg = (String) object;

				if (strMsg.equals(JMSUtil.START_BENCHMARK)) {
					System.out.println("Start benchmark - starting");
					started = true;
					startMainLoopThread();

				} else if (strMsg.equals(JMSUtil.STOP_BENCHMARK)) {
					System.out.println("Stop benchmark - stopping");
					started = false;
				} else {
					System.out.println("Unexpected string message");
				}
			} else if (object instanceof KundenbetreuerDTO) {

				System.out.println("received kundenbetreuer DTO");

				KundenbetreuerDTO dto = (KundenbetreuerDTO) object;
				
				sendMessage(dto.getGrossHandlerDTO().getHandlerDestination(),
						dto.getUhr());

			} else {
				System.out.println("Unknown message!");
			}

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
