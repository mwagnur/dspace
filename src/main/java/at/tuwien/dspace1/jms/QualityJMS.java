package at.tuwien.dspace1.jms;

import java.io.Serializable;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import at.tuwien.dspace1.jms.dto.AssemblyDTO;
import at.tuwien.dspace1.jms.dto.QualityDTO;
import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.Uhr;

public class QualityJMS implements MessageListener {
	private Connection connection;
	private static Session session;
	private ConnectionFactory connectionFactory;

	private Queue qualityQueue;
	private Queue serverQueue;
	private static MessageProducer senderServer;
	private MessageConsumer receiverQuality;

	private Destination tempDest;
	private MessageConsumer receiverResponse;

	private static final long MSG_TIMEOUT = 5000;

	private long workerId = -1;

	/**
	 * worker is started or still waits for a start signal
	 */
	private static boolean started = false;

	public QualityJMS(long id) {
		this.workerId = id;
	}

	public void init() throws JMSException, NamingException {

		Context jndiContext = new InitialContext();

		connectionFactory = (ConnectionFactory) jndiContext
				.lookup(JMSUtil.CONNECTION_FACTORY);

		connection = connectionFactory.createConnection();
		connection.setClientID(UUID.randomUUID().toString());

		// Create a Session
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		qualityQueue = (Queue) jndiContext.lookup(JMSUtil.QUALITY_QUEUE);
		serverQueue = (Queue) jndiContext.lookup(JMSUtil.SERVER_QUEUE);

		receiverQuality = session.createConsumer(qualityQueue);

		senderServer = session.createProducer(serverQueue);

		tempDest = session.createTemporaryQueue();
		receiverResponse = session.createConsumer(tempDest);
		receiverResponse.setMessageListener(this);

		Topic benchmarkTopicQueue;
		benchmarkTopicQueue = (Topic) jndiContext
				.lookup(JMSUtil.BENCHMARK_TOPIC_QUEUE);

		MessageConsumer subscriberTopic = session.createDurableSubscriber(
				benchmarkTopicQueue, "benchmark");
		subscriberTopic.setMessageListener(this);

		connection.start();
	}

	public void stop() throws JMSException {
		session.close();
		connection.close();
	}

	public void sendToServer(Serializable obj) throws JMSException {

		ObjectMessage message = session.createObjectMessage();
		message.setJMSReplyTo(tempDest);
		message.setObject(obj);
		senderServer.send(message);
		System.out.println("sent to server: " + obj);
	}

	public Message receiveFromServer() throws JMSException {
		return receiverQuality.receive(MSG_TIMEOUT);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
		}

		System.out.println("STARTING QUALITY WORKER");

		// TODO do the ID thing

		QualityJMS quality = new QualityJMS(Long.parseLong(args[0]));

		try {
			quality.init();

			
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}
	
	private void startMainLoopThread() throws JMSException {

		Thread foo = new Thread() {

			@Override
			public void run() {

				while (started) {
					System.out.println("checking for work..");

					try {
						QualityJMS.this.sendToServer(ServerJMS.CMD_QUALITY_REQUEST);
					} catch (JMSException e1) {
						e1.printStackTrace();
					}

					try {
						Thread.sleep(JMSUtil.WORKER_LOOP_SLEEP);

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println("Worker thread stopped");
			}			
		};
		foo.start();
		
	}

	private static void usage() {
		System.out.println("Usage: Quality <ID>");
		System.exit(-1);
	}

	public long getWorkerId() {
		return workerId;
	}

	public void setWorkerId(long workerId) {
		this.workerId = workerId;
	}

	@Override
	public void onMessage(Message answer) {

		ObjectMessage om = (ObjectMessage) answer;
		try {
			Serializable object = om.getObject();
			System.out.println("Received: " + object);

			if (object instanceof Uhr) {
				Uhr uhr = (Uhr) object;

				QualityDTO resp = checkClock(uhr);

				sendToServer(resp);

				System.out.println("checked and sent uhr: " + uhr);

			} else if (object instanceof String) {

				String msg = (String) object;

				if (msg.equals(JMSUtil.START_BENCHMARK)) {
					System.out.println("Start benchmark - starting");
					started = true;
					startMainLoopThread();
					
				} else if (msg.equals(JMSUtil.STOP_BENCHMARK)) {
					System.out.println("Stop benchmark - stopping");
					started = false;

				} else {
					System.out.println("Unexpected string message");
				}
			} else {
				System.out.println("Unexpected message");
			}
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private QualityDTO checkClock(Uhr uhr) {
		uhr.setQuality(new Random().nextInt(11));
		uhr.setQualityId(getWorkerId());

		QualityDTO resp = new QualityDTO(UUID.randomUUID(), uhr);
		return resp;
	}
}
