package at.tuwien.dspace1.jms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import at.tuwien.dspace1.jms.dto.AssInformationDTO;
import at.tuwien.dspace1.jms.dto.AssemblyDTO;
import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.Gehause;
import at.tuwien.dspace1.parts.KlassischeUhr;
import at.tuwien.dspace1.parts.LederArmband;
import at.tuwien.dspace1.parts.MetallArmband;
import at.tuwien.dspace1.parts.NormaleSportUhr;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.Uhrwerk;
import at.tuwien.dspace1.parts.Zeiger;
import at.tuwien.dspace1.parts.ZweiZonenSportUhr;
import at.tuwien.dspace1.shared.Util;

public class MontageJMS implements MessageListener {

	private Connection connection;
	private static Session session;
	private ConnectionFactory connectionFactory;

	private Queue montageQueue;
	private Queue serverQueue;

	private Destination tempDest;

	private MessageProducer senderServer;
	private MessageConsumer receiverMontage;
	private MessageConsumer receiverResponse;

	private static final long MSG_TIMEOUT = 5000;
	private long workerId = -1;

	private Random rand = new Random();
	private static LinkedList<String> serverRequests = new LinkedList<String>(); // TODO
																					// maybe
																					// remove
	/**
	 * working state of the montage worker (building a clock)
	 */
	private static boolean working = false;

	/**
	 * montage worker is started or still waits for a start signal
	 */
	private static boolean started = false;

	public MontageJMS(long id) {
		this.workerId = id;
		try {
			init();
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private void init() throws JMSException, NamingException {

		Context jndiContext = new InitialContext();

		connectionFactory = (ConnectionFactory) jndiContext
				.lookup(JMSUtil.CONNECTION_FACTORY);

		connection = connectionFactory.createConnection();
		connection.setClientID(UUID.randomUUID().toString());

		// Create a Session
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		// montageQueue = session.createQueue("TEST.FOO");

		montageQueue = (Queue) jndiContext.lookup(JMSUtil.MONTAGE_QUEUE);
		serverQueue = (Queue) jndiContext.lookup(JMSUtil.SERVER_QUEUE);

		tempDest = session.createTemporaryQueue();
		receiverResponse = session.createConsumer(tempDest);
		receiverResponse.setMessageListener(this);

		receiverMontage = session.createConsumer(montageQueue);

		senderServer = session.createProducer(serverQueue);

		Topic benchmarkTopicQueue;
		benchmarkTopicQueue = (Topic) jndiContext
				.lookup(JMSUtil.BENCHMARK_TOPIC_QUEUE);

		MessageConsumer subscriberTopic = session.createDurableSubscriber(
				benchmarkTopicQueue, "benchmark");
		subscriberTopic.setMessageListener(this);

		connection.start();
	}

	public void stop() throws JMSException {
		session.close();
		connection.close();
	}

	public void sendToServerWithRequest(Serializable obj) throws JMSException {

		ObjectMessage message = session.createObjectMessage();

		message.setJMSReplyTo(tempDest);
		String correlationId = this.createRandomString();
		message.setJMSCorrelationID(correlationId);
		serverRequests.add(correlationId);

		message.setObject(obj);
		senderServer.send(message);
		System.out.println("sent to server with request: " + obj);
	}

	public void sendToServer(Serializable obj) throws JMSException {

		ObjectMessage message = session.createObjectMessage();

		message.setJMSReplyTo(tempDest);
		message.setObject(obj);
		senderServer.send(message);
		// System.out.println("sent to server: " + obj);
	}

	public Message receiveFromServer() throws JMSException {
		return receiverMontage.receive(MSG_TIMEOUT);
	}

	private Uhr buildUhr(List<Bestandteil> bestandteile, long serialId) {
		LederArmband la = null;
		MetallArmband ma = null;
		Gehause g = null;
		Uhrwerk u = null;

		ArrayList<Zeiger> zeiger = new ArrayList<Zeiger>();

		for (Bestandteil curr : bestandteile) {

			if (curr instanceof LederArmband) {
				if (la != null) {
					System.out.println("Received wrong bestandteile");
				}
				la = (LederArmband) curr;
			} else if (curr instanceof MetallArmband) {
				if (ma != null) {
					System.out.println("Received wrong bestandteile");
				}
				ma = (MetallArmband) curr;
			} else if (curr instanceof Gehause) {
				if (g != null) {
					System.out.println("Received wrong bestandteile");
				}
				g = (Gehause) curr;
			} else if (curr instanceof Uhrwerk) {
				if (u != null) {
					System.out.println("Received wrong bestandteile");
				}
				u = (Uhrwerk) curr;
			} else if (curr instanceof Zeiger) {
				zeiger.add((Zeiger) curr);
			}

		}

		Uhr uhr = null;

		// Klassische Uhr: 1 LederArmband + 1 Gehause + 1 uhrwerk + 2 Zeiger
		if (la != null && ma == null && g != null && u != null) {

			if (zeiger.size() == 2) {
				uhr = new KlassischeUhr(la, g, u, zeiger.get(0), zeiger.get(1));
			}
		}
		// Sport Uhr: 1 MetallArmband + 1 Gehause + 1 uhrwerk + 2 Zeiger (+1
		// Zeiger ZweiZonenSportUhr)
		else if (ma != null && la == null && g != null && u != null) {

			if (zeiger.size() == 2) {

				uhr = new NormaleSportUhr(ma, g, u, zeiger.get(0),
						zeiger.get(1));
			} else if (zeiger.size() == 3) {
				uhr = new ZweiZonenSportUhr(ma, g, u, zeiger.get(0),
						zeiger.get(1), zeiger.get(2));
			}
		}

		simulateWorkingTime();

		if (uhr == null) {
			System.out.println("couldnt make clock - received wrong parts");
			return null;
		}
		uhr.setSerialNo(serialId);

		System.out.println("succesfully built Uhr: " + uhr);

		return uhr;
	}

	private void simulateWorkingTime() {
		if (!Util.debugMode()) {
			int time = rand.nextInt(3000);
			time = (time < 1000) ? 1000 : time;

			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				System.out.println("I WAS ASLEEP AND YOU INTERRUPTED ME");
			}
		}
	}

	public long getWorkerId() {
		return workerId;
	}

	public void setWorkerId(long workerId) {
		this.workerId = workerId;
	}

	private String createRandomString() {
		Random random = new Random(System.currentTimeMillis());
		long randomLong = random.nextLong();
		return Long.toHexString(randomLong);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
		}

		System.out.println("STARTING MONTAGE WORKER");

		MontageJMS montage = new MontageJMS(Long.parseLong(args[0]));

		System.out.println("STARTED MONTAGE WORKER - waiting for start signal");

	}

	private void startMainLoopThread() throws JMSException {

		Thread foo = new Thread() {

			@Override
			public void run() {
				while (started) {

					if (!working) {
						System.out.println("checking for work..");
						try {
							MontageJMS.this
									.sendToServer(ServerJMS.CMD_MONTAGE_REQUEST);

						} catch (JMSException e) {
							e.printStackTrace();
						}
					}

					try {
						Thread.sleep(JMSUtil.WORKER_LOOP_SLEEP);

					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
				System.out.println("Worker thread stopped");
			}
		};
		foo.start();

	}

	private static void usage() {
		System.out.println("Usage: Montage <ID>");
		System.exit(-1);
	}

	@Override
	public void onMessage(Message message) {

		ObjectMessage om = (ObjectMessage) message;

		Serializable object;
		try {
			object = om.getObject();

			System.out.println("Received: " + object);

			if (object instanceof AssemblyDTO) {

				working = true;
				AssemblyDTO ass = (AssemblyDTO) object;

				List<Bestandteil> bestandteile = ass.getBestandteile();

				Uhr uhr = buildUhr(bestandteile, ass.getSerialNo());
				uhr.setMontageId(getWorkerId());
				uhr.setContractNo(ass.getAuftragId());

				sendToServer(uhr);

				working = false;

			} else if (object instanceof AssInformationDTO) {
				// TODO NOt working atm
				AssInformationDTO ass = (AssInformationDTO) object;

				if (!working) {
					processAssemblyInformation(ass);
				}

			} else if (object instanceof String) {

				String msg = (String) object;

				if (msg.equals(JMSUtil.START_BENCHMARK)) {

					System.out.println("Start benchmark - starting");

					started = true;
					startMainLoopThread();

				} else if (msg.equals(JMSUtil.STOP_BENCHMARK)) {
					System.out.println("Stop benchmark - stopping");

					started = false;

				} else {
					System.out.println("Unexpected string message");
				}
			}

			else {
				System.out.println("Unexpected message");
			}

		} catch (JMSException e1) {
			e1.printStackTrace();
		}
	}

	private void processAssemblyInformation(AssInformationDTO ass) {
		ArrayList<Integer> possibilities = new ArrayList<Integer>();

		long gehause = ass.getGehauseCount();
		long uhrwerk = ass.getUhrwerkCount();
		long zeiger = ass.getZeigerCount();
		long lederArmband = ass.getLederArmbandCount();
		long metallArmband = ass.getMetallArmbandCount();

		if ((gehause > 0) && (uhrwerk > 0) && (zeiger > 1)) {
			if (lederArmband > 0) {
				// KlassischeUhr = 0q
				possibilities.add(0);
			}
			if (metallArmband > 0) {
				// NormaleSportUhr = 1
				possibilities.add(1);

				if (zeiger > 2) {
					// ZweiZonenSportUhr = 2
					possibilities.add(2);
				}
			}
		}

		if (possibilities.size() < 1) {
			/* NOTHING TO DO HERE */
			/* WE HAVE NO CHOICE */

			System.out.println("building a clock currently not possible.");

			return;
		}

		switch (possibilities.get(rand.nextInt(possibilities.size()))) {
		case 0:
			requestParts(new AssInformationDTO(1, 0, 2, 1, 1));
			break;
		case 1:
			requestParts(new AssInformationDTO(0, 1, 2, 1, 1));
			break;
		case 2:
			requestParts(new AssInformationDTO(0, 1, 3, 1, 1));
			break;
		default:

			break;
		}
	}

	private void requestParts(AssInformationDTO ass) {
		try {
			sendToServer(ass);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
