package at.tuwien.dspace1.jms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.InvalidDestinationException;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import at.tuwien.dspace1.jms.ServerDataManager.UhrTyp;
import at.tuwien.dspace1.jms.dto.AssInformationDTO;
import at.tuwien.dspace1.jms.dto.AssemblyDTO;
import at.tuwien.dspace1.jms.dto.GrossHandlerDTO;
import at.tuwien.dspace1.jms.dto.InformationDTO;
import at.tuwien.dspace1.jms.dto.KundenbetreuerDTO;
import at.tuwien.dspace1.jms.dto.LogistikDTO;
import at.tuwien.dspace1.jms.dto.QualityDTO;
import at.tuwien.dspace1.parts.Bestandteil;
import at.tuwien.dspace1.parts.BestandteilFactory;
import at.tuwien.dspace1.parts.Bestandteil.BestandteilTyp;
import at.tuwien.dspace1.parts.KlassischeUhr;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.shared.Auftrag;

public class ServerJMS implements MessageListener {

	public static final String CMD_MONTAGE_REQUEST = "montage_request";
	public static final String CMD_QUALITY_REQUEST = "quality_request";
	public static final String CMD_LOGISTIK_REQUEST = "logistik_request";
	public static final String CMD_LOGISTIK_AUSSCHUSS_REQUEST = "logistik_ausschuss_request";
	public static final String CMD_KUNDENBETREUER_REQUEST = "kundenbetreuer_request";
	public static final String CMD_REQUEST_FAILED = "nope";

	private MessageConsumer serverReceiver;
	private Connection connection;
	private Session session;
	private Context jndiContext;

	private MessageProducer topicPublisher;
	private InformationDTO information;

	private ServerDataManager dataManager;

	private Map<String, GrossHandlerDTO> grossHaendler = new HashMap<String, GrossHandlerDTO>();
	private ArrayList<String> grossHaendlerURIs = new ArrayList<String>();

	public ServerJMS() {
		dataManager = new ServerDataManager();
		information = new InformationDTO();

	}

	public ServerDataManager getDataManager() {
		return dataManager;
	}

	public void init() throws JMSException, NamingException {
		System.out.println("INIT SERVER");

		jndiContext = new InitialContext();

		ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext
				.lookup(JMSUtil.CONNECTION_FACTORY);

		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		connectionFactory = (ConnectionFactory) jndiContext
				.lookup("ConnectionFactory");
		Queue serverQueue = (Queue) jndiContext.lookup(JMSUtil.SERVER_QUEUE);

		Topic clusterTopic = (Topic) jndiContext.lookup(JMSUtil.UI_TOPIC_QUEUE);
		topicPublisher = session.createProducer(clusterTopic);

		serverReceiver = session.createConsumer(serverQueue);
		serverReceiver.setMessageListener(this);

		connection.start();

	}

	public void stop() throws JMSException {
		session.close();
		connection.close();

		System.out.println("Server stopped!");
	}

	private void sendMessage(String queue, Serializable msg) {
		try {

			Queue receivingQueue = (Queue) jndiContext.lookup(queue);
			ConnectionFactory factory = (ConnectionFactory) jndiContext
					.lookup(JMSUtil.CONNECTION_FACTORY);

			Connection connection = factory.createConnection();
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			MessageProducer sender = session.createProducer(receivingQueue);

			ObjectMessage message = session.createObjectMessage();
			message.setObject(msg);
			sender.send(message);

			connection.close();
			System.out.println("Message sent to " + queue);

		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	private void sendResponseMessage(Message respondTo, Serializable msg) {
		try {

			ConnectionFactory factory = (ConnectionFactory) jndiContext
					.lookup(JMSUtil.CONNECTION_FACTORY);

			Connection connection = factory.createConnection();
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			MessageProducer sender = session.createProducer(respondTo
					.getJMSReplyTo());

			ObjectMessage message = session.createObjectMessage();
			message.setJMSCorrelationID(respondTo.getJMSCorrelationID());
			message.setObject(msg);
			sender.send(message);

			connection.close();
			System.out.println("Message sent as respond ");

		} catch (NamingException e) {
			e.printStackTrace();
		} catch (InvalidDestinationException id) {
			System.out
					.println("Couldn't respond to message, connection closed");
			// TODO DestinationDoesNotExistException gets still thrown when
			// workers temporary queue closed but server still has enqueud
			// messages he wants to reply to (no problem for functionality, but
			// not very nice)
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to publish current content of containers to all topic subscribers
	 * (UI)
	 */
	private void publishInformation() {

		// TODO for benchmarking to get performance
		// if(!JMSUtil.benchmarkMode){
		sendTopicMessage(getInformationFromData());
		// }
	}

	private void sendTopicMessage(Serializable msg) {
		try {
			ObjectMessage message = session.createObjectMessage();
			message.setObject(msg);

			topicPublisher.send(message);
			System.out.println("Publishing TopicMessage to topic subscribers");

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	private void sendMessage(Destination destination, Serializable msg) {
		try {

			ConnectionFactory factory = (ConnectionFactory) jndiContext
					.lookup(JMSUtil.CONNECTION_FACTORY);

			Connection connection = factory.createConnection();
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			MessageProducer sender = session.createProducer(destination);

			ObjectMessage message = session.createObjectMessage();
			message.setObject(msg);
			sender.send(message);

			connection.close();
			System.out.println("Message sent to destination: " + destination);

		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	private InformationDTO getInformationFromData() {

		information.setLederArmbandCount(dataManager
				.getBestandteilCount(BestandteilTyp.LEDERARMBAND));
		information.setMetallArmbandCount(dataManager
				.getBestandteilCount(BestandteilTyp.METALLARMBAND));
		information.setUhrwerkCount(dataManager
				.getBestandteilCount(BestandteilTyp.UHRWERK));
		information.setGehauseCount(dataManager
				.getBestandteilCount(BestandteilTyp.GEHAUSE));
		information.setZeigerCount(dataManager
				.getBestandteilCount(BestandteilTyp.ZEIGER));

		List<Uhr> quality = dataManager.getClocksFromQuality();
		List<Uhr> logistik = dataManager.getClocksFromLogistik();
		List<Uhr> delivered = dataManager.getClocksFromDelivered();
		List<Uhr> trash = dataManager.getClocksFromTrash();

		information.setQualityClockList(quality);
		information.setQualityClockCount(quality.size());

		information.setLogistikClockList(logistik);
		information.setLogistikClockCount(logistik.size());

		information.setFinalClockList(delivered);
		information.setFinalClockCount(delivered.size());

		information.setTrashClockList(trash);
		information.setTrashClockCount(trash.size());

		information.setActiveAuftragList(dataManager.getActiveAuftraege());
		information
				.setCompletedAuftragList(dataManager.getCompletedAuftraege());

		return information;
	}

	/**
	 * @param args
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {

		// TODO correct IDs with args for montage/quality/logistik etc

		try {
			ServerJMS server = new ServerJMS();
			server.init();

			System.out.println("PRESS ENTER TO QUIT");

			new Scanner(System.in).hasNextLine();

			server.getDataManager().printAllContainersDebug();

			server.stop();
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onMessage(Message receivedMessage) {
		ObjectMessage om = (ObjectMessage) receivedMessage;

		Serializable object;
		try {
			object = om.getObject();

			if (object instanceof String) {
				// System.out.println("Received string message: " + object);
				String message = (String) object;

				if (message.equals(CMD_MONTAGE_REQUEST)) {

					AssemblyDTO response;
					if (JMSUtil.benchmarkMode) {
						response = dataManager
								.takePartsFromAssemblyWithoutContracts();
					} else {
						response = dataManager
								.takePartsFromAssemblyWithContracts();
					}

					if (response == null) {
						System.out
								.println("No clock is buildable at the moment");
						return;
					}
					sendResponseMessage(receivedMessage, response);

				} else if (message.equals(CMD_QUALITY_REQUEST)) {

					Uhr responseClock = dataManager.takeClockFromQuality();

					if (responseClock == null) {
						System.out
								.println("no Uhren available for quality check; send request failed");
					} else {

						publishInformation();
						sendResponseMessage(receivedMessage, responseClock);
					}

				} else if (message.equals(CMD_LOGISTIK_REQUEST)) {

					int gueteFrom = om
							.getIntProperty(LogistikJMS.LOGISTIK_GUETE_FROM_PROPERTY);
					int gueteTo = om
							.getIntProperty(LogistikJMS.LOGISTIK_GUETE_TO_PROPERTY);

					Uhr responseClock = dataManager.takeClockFromLogistik(
							gueteFrom, gueteTo);

					if (responseClock == null) {
						System.out
								.println("no Guete Uhren available for logistik transportation; send request failed");
						sendResponseMessage(receivedMessage, CMD_REQUEST_FAILED);
					} else {

						publishInformation();

						sendResponseMessage(receivedMessage, responseClock);
					}

				} else if (message.equals(CMD_LOGISTIK_AUSSCHUSS_REQUEST)) {

					int gueteFrom = 0;
					int gueteTo = 2;

					Uhr responseClock = dataManager.takeClockFromLogistik(
							gueteFrom, gueteTo);

					if (responseClock == null) {
						System.out
								.println("no trash Uhren available for transport either;");
					} else {

						publishInformation();

						sendResponseMessage(receivedMessage, responseClock);
					}

				} else if (message.equals(CMD_KUNDENBETREUER_REQUEST)) {

					KundenbetreuerDTO dto = processKundenBetreuung();
					
					if(dto==null){
						System.out.println("no uhren or grosshaendler available for kundenbetreuer");
						return;
					}
					
					sendResponseMessage(receivedMessage, dto);

				}

				else if (message.equals(JMSUtil.PREPARE_BENCHMARK_MESSAGE)) {

					prepareBenchmark();

					System.out.println("SUCCESSFULLY PREPARED BENCHMARK YEY");

					publishInformation();
				}

			} else if (object instanceof Bestandteil) {
				// RECEIVED BESTANDTEIL

				// System.out.println("Received Bestandteil: " + object);

				Bestandteil part = (Bestandteil) object;

				dataManager.addPartToAssembly(part);

				publishInformation();

			} else if (object instanceof AssInformationDTO) {
				// System.out.println("Received montage request: " + object);

				AssInformationDTO ass = (AssInformationDTO) object;

				// TODO REWOWKRKKKK

				List<Bestandteil> list = dataManager.takePartsFromAssembly(
						(int) ass.getGehauseCount(),
						(int) ass.getUhrwerkCount(),
						(int) ass.getZeigerCount(),
						(int) ass.getLederArmbandCount(),
						(int) ass.getMetallArmbandCount());

				if (list == null) {
					System.out
							.println("not enough parts found for an AssemblyDTO; send request failed");

				} else {

					AssemblyDTO response = new AssemblyDTO(UUID.randomUUID(),
							list);
					response.setSerialNo(dataManager.getAndIncrSerialNo());

					publishInformation();

					sendResponseMessage(receivedMessage, response);
				}

			} else if (object instanceof Uhr) {
				// RECEIVED UHR FROM MONTAGE - ADD TO QUALITY

				// System.out.println("Received Uhr: " + object);

				Uhr uhr = (Uhr) object;

				dataManager.addClockToQuality(uhr);

				publishInformation();

			} else if (object instanceof QualityDTO) {
				// RECEIVED FROM QUALITY CONTROL - ADD TO LOGISTIK

				// System.out.println("Received Quality: " + object);

				QualityDTO checked = (QualityDTO) object;

				dataManager.addClockToLogistik(checked.getUhr());

				publishInformation();

			} else if (object instanceof LogistikDTO) {
				// RECEIVED FROM LOGISTIK - ADD TO DELIVERED

				// System.out.println("Received Logistik: " + object);

				LogistikDTO log = (LogistikDTO) object;

				if (log.isTrash()) {
					// its trash!

					dataManager.addClockToTrash(log.getFinalUhr());

					publishInformation();

				} else {
					// its a final clock!
					dataManager.addClockToDelivered(log.getFinalUhr());

					publishInformation();
				}

			} else if (object instanceof Auftrag) {
				Auftrag auftrag = (Auftrag) object;

				dataManager.addAuftrag(auftrag);

				publishInformation();
			} else if (object instanceof GrossHandlerDTO) {

				GrossHandlerDTO gross = (GrossHandlerDTO) object;

				if (!grossHaendler.containsKey(gross.getHandlerURI())) {
					System.out.println("new grosshaendler with uri: "
							+ gross.getHandlerURI());

					grossHaendler.put(gross.getHandlerURI(), gross);
					grossHaendlerURIs.add(gross.getHandlerURI());

				} else {
					System.out.println("received grosshandler update - "
							+ gross);
					grossHaendler.remove(gross.getHandlerURI());
					grossHaendler.put(gross.getHandlerURI(), gross);

					System.out.println(grossHaendler);
				}
			}

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	private KundenbetreuerDTO processKundenBetreuung() {
		Uhr uhr;
		for (String currUri : grossHaendler.keySet()) {

			GrossHandlerDTO currDTO = grossHaendler.get(currUri);

			if (currDTO.getKlassischeUhrenCurrent() < currDTO
					.getKlassischeUhrenMax()) {
				uhr = dataManager.getUhrForGrossHaendler(UhrTyp.KLASSISCH);

				if (uhr != null) {
					return buildKundenbetreuerDTO(uhr, currDTO);
				}
			}

			if (currDTO.getNormaleSportUhrenCurrent() < currDTO
					.getNormaleSportUhrenMax()) {
				uhr = dataManager
						.getUhrForGrossHaendler(UhrTyp.NORMAL_SPORTLICH);

				if (uhr != null) {
					return buildKundenbetreuerDTO(uhr, currDTO);
				}
			}

			if (currDTO.getZweiZonenSportUhrenCurrent() < currDTO
					.getZweiZonenSportUhrenMax()) {
				uhr = dataManager
						.getUhrForGrossHaendler(UhrTyp.ZWEIZONEN_SPORTLICH);

				if (uhr != null) {
					return buildKundenbetreuerDTO(uhr, currDTO);
				}
			}
		}

		return null;
	}
	
	private KundenbetreuerDTO buildKundenbetreuerDTO(Uhr uhr, GrossHandlerDTO currDTO){
			
		return new KundenbetreuerDTO(uhr, currDTO);
		
	}

	private void prepareBenchmark() {
		for (int i = 0; i < 1500; i++) {
			dataManager.addPartToAssembly(BestandteilFactory.createPart(
					UUID.randomUUID(), BestandteilTyp.GEHAUSE));
		}
		for (int i = 0; i < 1500; i++) {
			dataManager.addPartToAssembly(BestandteilFactory.createPart(
					UUID.randomUUID(), BestandteilTyp.UHRWERK));
		}
		for (int i = 0; i < 750; i++) {
			dataManager.addPartToAssembly(BestandteilFactory.createPart(
					UUID.randomUUID(), BestandteilTyp.LEDERARMBAND));
		}
		for (int i = 0; i < 750; i++) {
			dataManager.addPartToAssembly(BestandteilFactory.createPart(
					UUID.randomUUID(), BestandteilTyp.METALLARMBAND));
		}
		for (int i = 0; i < 3750; i++) {
			dataManager.addPartToAssembly(BestandteilFactory.createPart(
					UUID.randomUUID(), BestandteilTyp.ZEIGER));
		}
	}

}
