package at.tuwien.dspace1.jms;

import java.io.Serializable;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import at.tuwien.dspace1.jms.dto.LogistikDTO;
import at.tuwien.dspace1.parts.Uhr;
import at.tuwien.dspace1.parts.ZweiZonenSportUhr;

public class LogistikJMS implements MessageListener {
	private Connection connection;
	private static Session session;
	private ConnectionFactory connectionFactory;

	private static MessageProducer senderServer;
	private MessageConsumer receiverLogistik;

	private static final long MSG_TIMEOUT = 10000;

	public final static String LOGISTIK_GUETE_FROM_PROPERTY = "guete_from_prop";
	public final static String LOGISTIK_GUETE_TO_PROPERTY = "guete_to_prop";

	private static int from;
	private final int to;

	private Destination tempDest;
	private MessageConsumer receiverResponse;

	public enum LogType {
		A, B
	};

	private LogType type;
	private long id;

	/**
	 * worker is started or still waits for a start signal
	 */
	private static boolean started = false;

	public LogistikJMS(long id, LogType b) {
		this.id = id;
		this.type = b;

		if (type.equals(LogType.A)) {
			from = 8;
			to = 10;
		} else {
			from = 3;
			to = 7;
		}
	}

	public void init() throws JMSException, NamingException {

		Context jndiContext = new InitialContext();

		connectionFactory = (ConnectionFactory) jndiContext
				.lookup(JMSUtil.CONNECTION_FACTORY);

		connection = connectionFactory.createConnection();
		connection.setClientID(UUID.randomUUID().toString());

		// Create a Session
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		Queue logistikQueue = (Queue) jndiContext
				.lookup(JMSUtil.LOGISTIK_QUEUE);
		Queue serverQueue = (Queue) jndiContext.lookup(JMSUtil.SERVER_QUEUE);

		receiverLogistik = session.createConsumer(logistikQueue);
		senderServer = session.createProducer(serverQueue);

		tempDest = session.createTemporaryQueue();
		receiverResponse = session.createConsumer(tempDest);
		receiverResponse.setMessageListener(this);

		Topic benchmarkTopicQueue;
		benchmarkTopicQueue = (Topic) jndiContext
				.lookup(JMSUtil.BENCHMARK_TOPIC_QUEUE);

		MessageConsumer subscriberTopic = session.createDurableSubscriber(
				benchmarkTopicQueue, "benchmark");
		subscriberTopic.setMessageListener(this);

		connection.start();
	}

	public void stop() throws JMSException {
		session.close();
		connection.close();
	}

	public void sendToServer(Serializable obj) throws JMSException {

		ObjectMessage message = session.createObjectMessage();

		message.setIntProperty(LOGISTIK_GUETE_FROM_PROPERTY, from);
		message.setIntProperty(LOGISTIK_GUETE_TO_PROPERTY, to);

		message.setJMSReplyTo(tempDest);
		message.setObject(obj);
		senderServer.send(message);
		System.out.println("sent to server: " + obj);
	}

	public Message receiveFromServer() throws JMSException {
		return receiverLogistik.receive(MSG_TIMEOUT);
	}

	public static void main(String[] args) {

		

		if (args.length != 2) {
			usage();
		}
		if (!(args[1].equalsIgnoreCase("A"))
				&& !(args[1].equalsIgnoreCase("B"))) {
			usage();
		}

		System.out.println("STARTING LOGISTIK WORKER");
		
		LogistikJMS logistik;
		if (args[1].equalsIgnoreCase("B")) {
			logistik = new LogistikJMS(Long.parseLong(args[0]), LogType.B);
		} else {
			logistik = new LogistikJMS(Long.parseLong(args[0]), LogType.A);
		}

		try {

			logistik.init();

		

		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private void startMainLoopThread() throws JMSException {

		Thread foo = new Thread() {

			@Override
			public void run() {

				while (started) {
					System.out.println("checking for work..");

					try {
						LogistikJMS.this.sendToServer(ServerJMS.CMD_LOGISTIK_REQUEST);
						
					} catch (JMSException e1) {
						e1.printStackTrace();
					}

					try {
						Thread.sleep(JMSUtil.WORKER_LOOP_SLEEP);

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println("Worker thread stopped");
			}			
		};
		foo.start();
		
	}

	private static void usage() {
		System.out.println("Usage: Logistik <ID> <typ>");
		System.exit(-1);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public void onMessage(Message message) {

		ObjectMessage om = (ObjectMessage) message;
		try {
			Serializable object = om.getObject();
			System.out.println("Received: " + object);

			if (object instanceof String) {
				System.out.println("Received string message: " + object);
				String strMsg = (String) object;
				if (strMsg.equals(ServerJMS.CMD_REQUEST_FAILED)) {

					System.out
							.println("Server answered: request of guete-clock failed; request trash clock");

					this.sendToServer(ServerJMS.CMD_LOGISTIK_AUSSCHUSS_REQUEST);
				} else if (strMsg.equals(JMSUtil.START_BENCHMARK)) {
					System.out.println("Start benchmark - starting");
					started = true;
					startMainLoopThread();
					
				} else if (strMsg.equals(JMSUtil.STOP_BENCHMARK)) {
					System.out.println("Stop benchmark - stopping");
					started = false;
				} else {
					System.out.println("Unexpected string message");
				}
			} else if (object instanceof Uhr) {
				Uhr clock = (Uhr) object;

				LogistikDTO finalDelivery = new LogistikDTO(UUID.randomUUID(),
						clock);

				if (clock.getQuality() <= 2) {
					finalDelivery.setTrash(true);

					if (clock instanceof ZweiZonenSportUhr) {
						ZweiZonenSportUhr spClock = (ZweiZonenSportUhr) clock;
						this.sendToServer(spClock.getArmband());
						this.sendToServer(spClock.getGehause());
						this.sendToServer(spClock.getZeig1());
						this.sendToServer(spClock.getZeig2());
						this.sendToServer(spClock.getZeiger3());
					} else {
						this.sendToServer(clock.getArmband());
						this.sendToServer(clock.getGehause());
						this.sendToServer(clock.getZeig1());
						this.sendToServer(clock.getZeig2());
					}

				} else {
					finalDelivery.setTrash(false);
				}
				this.sendToServer(finalDelivery);

				System.out.println("delivering: " + clock);
			} else {
				System.out.println("Unexpected message");
			}

		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}
